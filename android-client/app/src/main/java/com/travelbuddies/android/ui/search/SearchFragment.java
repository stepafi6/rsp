package com.travelbuddies.android.ui.search;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.Destination;
import com.travelbuddies.android.backendBridge.model.Event;
import com.travelbuddies.android.backendBridge.model.SearchEventRequestBody;
import com.travelbuddies.android.backendBridge.providers.DestinationProvider;
import com.travelbuddies.android.backendBridge.providers.EventProvider;
import com.travelbuddies.android.utils.Global;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class SearchFragment extends Fragment {

    private Button buttonFind;
    private Button buttonCreateEvent;
    private TextView textViewDate;

    private AutoCompleteTextView editTextPlace;
    private DestinationProvider destinationProvider;
    private List<Destination> hints;
    private List<String> strings = new ArrayList<>();
    private ArrayAdapter arrayAdapter;
    // set to a random value to recognize difference
    private String string = "init";
    private List<Destination> destinations = new ArrayList<>();

    private DatePickerDialog.OnDateSetListener DateSetListener;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.search_fragment, container, false);
        destinationProvider = new DestinationProvider(getContext());
        buttonFind = root.findViewById(R.id.buttonFind);
        textViewDate = root.findViewById(R.id.textViewDate);
        editTextPlace = root.findViewById(R.id.editTextPlace);
        buttonCreateEvent = root.findViewById(R.id.buttonCreateEvent);

        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
        textViewDate.setText(date);

        datePick();

        arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, strings);
        arrayAdapter.setNotifyOnChange(true);
        editTextPlace.setThreshold(1);
        editTextPlace.setAdapter(arrayAdapter);

        editTextPlace.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string2 = editTextPlace.getText().toString();
                if (string.equals(string2)) return;
                string = string2;
                editTextPlace.dismissDropDown();
                if (s.length() != 0) {
                    hintMeResults(editTextPlace.getText().toString());
                }
            }
        });

        buttonCreateEvent.setOnClickListener(v -> startActivity(new Intent(getActivity().getApplicationContext(), CreateEventActivity.class)));
        buttonFind.setOnClickListener(v -> sendData());

        return root;
    }

    private void hintMeResults(String text) {
        destinationProvider.suggestDestinations(new Callback<List<Destination>>() {

            @Override
            public void onResponse(Call<List<Destination>> call, Response<List<Destination>> response) {
                hints = response.body();
                destinations.addAll(hints);
                if (hints == null) return;
                List<String> autoFill = hints.stream().map(Destination::getCity).collect(Collectors.toList());

                // To force autofill to show correct results, we had to refresh the input and reload adapter
                arrayAdapter.clear();
                arrayAdapter.addAll(autoFill);
                forceReload();
                editTextPlace.showDropDown();
            }

            @Override
            public void onFailure(Call<List<Destination>> call, Throwable t) {
                Log.i("SearchFragment", "Can not get data");
            }
        }, text);
    }

    private void forceReload() {
        editTextPlace.setText(editTextPlace.getText().toString());
        editTextPlace.setSelection(editTextPlace.getText().toString().length());
    }

    private void datePick() {
        textViewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        getActivity(),
                        R.style.DialogTheme,
                        DateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.show();
            }
        });

        DateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + year + "-" + month + "-" + day);

                String date = year + "-" + month + "-" + day;
                textViewDate.setText(date);
            }
        };
    }

    private void sendData() {
        String when = textViewDate.getText().toString();
        Date date = new Date();
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(when);
        } catch (ParseException e) {
            e.printStackTrace();
            return;
        }

        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        int year = localDate.getYear();
        int month = localDate.getMonthValue();
        int day = localDate.getDayOfMonth();

        String where = editTextPlace.getText().toString();
        Optional<Destination> destination = destinations.stream().filter(x -> x.getCity().equals(where)).findAny();
        if (!destination.isPresent()) {
            Log.e("SearchFragment", "destination doesn't match anything from received");
            Toast.makeText(getActivity().getApplicationContext(), "Wrong Destination", Toast.LENGTH_SHORT).show();
            return;
        }
        SearchEventRequestBody request = new SearchEventRequestBody(destination.get(), year, month - 1, day);
        EventProvider eventProvider = new EventProvider(getActivity().getApplicationContext());
        eventProvider.getEventsFromSearch(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                Log.i("SearchFragment", "Got searched data.");
                ((Global) getActivity().getApplicationContext()).setSearchEvents(response.body());
                startActivity(new Intent(getActivity().getApplicationContext(), AfterSearchActivity.class));
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                Log.e("SearchFragment", "Server response for getting event via search is failed.");
            }
        }, request);
    }
}