package com.travelbuddies.android.backendBridge.model;

public class Destination {

    private int geonameId;
    private String city;
    private String subcountry;

    private Country country;

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Destination(int geonameId, String city, String subcountry, Country country) {
        this.geonameId = geonameId;
        this.city = city;
        this.subcountry = subcountry;
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getGeonameId() {
        return geonameId;
    }

    public void setGeonameId(int geonameId) {
        this.geonameId = geonameId;
    }

    public String getSubcountry() {
        return subcountry;
    }

    public void setSubcountry(String subcountry) {
        this.subcountry = subcountry;
    }


}
