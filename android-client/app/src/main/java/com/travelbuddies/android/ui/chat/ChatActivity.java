package com.travelbuddies.android.ui.chat;

import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.Message;
import com.travelbuddies.android.backendBridge.model.User;
import com.travelbuddies.android.backendBridge.providers.MessageProvider;
import com.travelbuddies.android.factory.ChatFactory;
import com.travelbuddies.android.utils.Global;

import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity {

    private FloatingActionButton back;
    private LinearLayout msgContainer;
    private ChatFactory chatFactory;
    private EditText input;
    private ImageButton send;
    private MessageProvider provider;
    private long id;
    private TextView chatName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        msgContainer = findViewById(R.id.msgContainer);
        chatFactory = new ChatFactory(getApplicationContext());
        provider = new MessageProvider(getApplicationContext());
        User user = ((Global) getApplicationContext()).getUser();
        id = Objects.requireNonNull(getIntent().getExtras()).getLong("id");

        // Just to make keyboard not to break the layout
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        back = findViewById(R.id.backFromMsg);
        input = findViewById(R.id.input);
        send = findViewById(R.id.send);
        chatName = findViewById(R.id.friendName);

        chatName.setText(getIntent().getExtras().getString("name"));

        send.setOnClickListener(v -> {
            provider.sendMessage(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Log.i("ChatActivity", "msg sent" + response.code());
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.i("ChatActivity", "failed to send a msg");
                }
            }, id, input.getText().toString());
            msgContainer.addView(chatFactory.getMessageContainer(user.getPicture(), new Timestamp(System.currentTimeMillis()), input.getText().toString()));
            input.setText("");
        });

        // go back
        back.setOnClickListener(v -> onBackPressed());

        // Retrieving msgs from state holder
        List<Message> messages = ((Global) getApplicationContext()).getMessages();

        for (Message message : messages) {
            msgContainer.addView(chatFactory.getMessageContainer(message.systemUser.getPicture(), message.getTimestamp(), message.getContent()));
        }
    }
}
