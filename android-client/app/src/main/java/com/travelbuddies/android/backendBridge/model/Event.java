package com.travelbuddies.android.backendBridge.model;

import java.util.Date;
import java.util.List;

public class Event {

    long id;

    private String description;
    private Date fromDate;
    private Date toDate;
    private Destination destination;

    private User author;

    private List<User> participants;
    private boolean deleted = false;

    public Event(String description, Date fromDate, Date toDate, Destination destination, User author, List<User> participants, boolean deleted) {
        this.description = description;
        this.fromDate = fromDate;
        this.toDate = toDate;
        this.destination = destination;
        this.author = author;
        this.participants = participants;
        this.deleted = deleted;
    }

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date from) {
        this.fromDate = from;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date to) {
        this.toDate = to;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<User> getParticipants() {
        return participants;
    }

    public void setParticipants(List<User> participants) {
        this.participants = participants;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", fromDate=" + fromDate +
                ", toDate=" + toDate +
                ", destination=" + destination +
                ", author=" + author +
                ", participants=" + participants +
                ", deleted=" + deleted +
                '}';
    }
}
