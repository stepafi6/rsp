package com.travelbuddies.android.backendBridge.providers;

import android.content.Context;
import android.content.SharedPreferences;

import com.travelbuddies.android.backendBridge.services.ServiceGenerator;

public abstract class AbstractProvider<T> {

    protected T service;

    public AbstractProvider(Context context, Class<T> clazz) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("secrets", Context.MODE_PRIVATE);
        String accessToken = sharedPreferences.getString("accessToken", "");
        //Create service with the token
        this.service = ServiceGenerator.createService(clazz, accessToken);
    }
}
