package com.travelbuddies.android.backendBridge.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

public class User {

    @SerializedName("sub")
    String id;

    String givenName;

    String familyName;
    String nickname;
    String name;
    String picture;

    String updatedAt;
    String email;

    boolean emailVerified;

    Set<User> friends;

    List<Country> visitedCountries;

    public User(String id, String givenName, String familyName, String nickname, String name, String picture, String updatedAt, String email, boolean emailVerified, Set<User> friends, List<Country> visitedCountries) {
        this.id = id;
        this.givenName = givenName;
        this.familyName = familyName;
        this.nickname = nickname;
        this.name = name;
        this.picture = picture;
        this.updatedAt = updatedAt;
        this.email = email;
        this.emailVerified = emailVerified;
        this.friends = friends;
        this.visitedCountries = visitedCountries;
    }

    public String getId() {
        return id;
    }

    public void setId(String sub) {
        this.id = sub;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public Set<User> getFriends() {
        return friends;
    }

    public void setFriends(Set<User> friends) {
        this.friends = friends;
    }

    public List<Country> getVisitedCountries() {
        return visitedCountries;
    }

    public void setVisitedCountries(List<Country> visitedCountries) {
        this.visitedCountries = visitedCountries;
    }

    public void addFriend(User user) {
        Objects.requireNonNull(user);
        if (friends == null) {
            this.friends = new HashSet<>();
        }
        friends.add(user);
    }

    public void removeFriend(User user) {
        Objects.requireNonNull(user);
        if (friends == null) {
            return;
        }
        friends.removeIf(u -> Objects.equals(u.getId(), user.getId()));
    }

    public void addVisitedCountry(Country country) {
        Objects.requireNonNull(country);
        if (visitedCountries == null) {
            this.visitedCountries = new ArrayList<>();
        }
        visitedCountries.add(country);
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
