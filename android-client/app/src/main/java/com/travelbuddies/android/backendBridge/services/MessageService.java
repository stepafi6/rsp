package com.travelbuddies.android.backendBridge.services;

import com.travelbuddies.android.backendBridge.model.Conversation;
import com.travelbuddies.android.backendBridge.model.Message;
import com.travelbuddies.android.backendBridge.model.User;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface MessageService {

    @POST("/api/conversations/")
    Call<ResponseBody> createConversation(@Body List<User> users);

    @GET("/api/conversations/{conversationId}")
    Call<List<Message>> getMessagesFromConversation(@Path("conversationId") long conversationId);

    @GET("/api/conversations/")
    Call<List<Conversation>> getConversationsOfCurrentUser();

    @PUT("/api/conversations/{conversationId}")
    Call<ResponseBody> sendMessage(@Path("conversationId") long conversationId, @Body RequestBody message);

}
