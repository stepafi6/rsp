package com.travelbuddies.android.factory;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.User;
import com.travelbuddies.android.backendBridge.providers.UserProvider;
import com.travelbuddies.android.utils.Global;

import java.util.stream.Collectors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewFriendFactory extends ChiefFactory {
    public NewFriendFactory(Context context) {
        super(context);
    }

    public View getUserView(String picture, User user) {
        // Initializing the master
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.user_container, null);

        // Targeting children
        ImageView imageView = (ImageView) ((LinearLayout) linearLayout.getChildAt(0)).getChildAt(0);
        TextView textView = (TextView) ((LinearLayout) linearLayout.getChildAt(0)).getChildAt(1);

        // set data
        Picasso.get().load(picture).resize(65, 65).into(imageView);
        textView.setText(user.getName());

        linearLayout.setOnClickListener(v -> {
            if ((!((Global) context).getFriendsOfCurrentUser().stream().map(x -> x.getId()).collect(Collectors.toList()).contains(user.getId())) && !(user.getId().equals(((Global) context).getUser().getId()))) {
                Snackbar.make(linearLayout, "Want to add this friend?", Snackbar.LENGTH_LONG).setDuration(5000)
                        .setAction("Yes", v1 -> {
                            UserProvider userProvider = new UserProvider(context);
                            userProvider.addFriend(new Callback<User>() {
                                @Override
                                public void onResponse(Call<User> call, Response<User> response) {
                                    Log.i("NewFriendFactory", "Friend added");
                                    Toast.makeText(context, "Friend added", Toast.LENGTH_SHORT).show();
                                    ((Global) context).getFriendsOfCurrentUser().add(user);
                                }

                                @Override
                                public void onFailure(Call<User> call, Throwable t) {
                                    Log.e("NewFriendFactory", "Failed to add friend!");
                                }
                            }, user.getId());
                        }).show();
            } else if (user.getId().equals(((Global) context).getUser().getId())) {
                Toast.makeText(context, "You cannot add yourself!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "You have already added this friend!", Toast.LENGTH_SHORT).show();
            }
        });

        return linearLayout;
    }
}
