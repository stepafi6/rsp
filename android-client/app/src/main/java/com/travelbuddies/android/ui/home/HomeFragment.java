package com.travelbuddies.android.ui.home;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;
import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.Event;
import com.travelbuddies.android.backendBridge.model.User;
import com.travelbuddies.android.factory.HomeFactory;
import com.travelbuddies.android.ui.Friend.NewFriendActivity;
import com.travelbuddies.android.ui.events.MyEventsActivity;
import com.travelbuddies.android.utils.Global;

import java.util.List;
import java.util.Objects;

public class HomeFragment extends Fragment {

    private HomeFactory factory;

    private TextView textViewWelcome;
    private LinearLayout linearLayoutBuddies;
    private TextView textViewLocation;
    private TextView textViewDate;
    private TextView textViewBuddies;
    private ImageView imageViewBuddy1;
    private ImageView imageViewBuddy2;
    private ImageView imageViewCreator;
    private ImageButton imageButtonAddFriend;
    private Button buttonShowAll;

    private CoordinatorLayout coordinatorLayout;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        factory = new HomeFactory(getActivity().getApplicationContext());

        textViewWelcome = root.findViewById(R.id.textViewWelcome);
        linearLayoutBuddies = root.findViewById(R.id.linearLayoutBuddies);
        textViewLocation = root.findViewById(R.id.textViewLocation);
        textViewDate = root.findViewById(R.id.textViewDate);
        imageViewBuddy1 = root.findViewById(R.id.imageViewBuddy1);
        imageViewBuddy2 = root.findViewById(R.id.imageViewBuddy2);
        imageViewCreator = root.findViewById(R.id.imageViewCreator);
        imageButtonAddFriend = root.findViewById(R.id.imageButtonAddFriend);
        textViewBuddies = root.findViewById(R.id.textViewBuddies);
        buttonShowAll = root.findViewById(R.id.buttonShowAll);
        coordinatorLayout = root.findViewById(R.id.coordinator);

        imageButtonAddFriend.setBackgroundColor(Color.TRANSPARENT);
        imageButtonAddFriend.setOnClickListener(v -> startActivity(new Intent(getContext(), NewFriendActivity.class)));
        buttonShowAll.setOnClickListener(v -> startActivity(new Intent(getContext(), MyEventsActivity.class)));

        drawScreen();

        return root;
    }


    private void drawScreen() {
        // Welcome header
        String welcomeMsg = "Welcome back, "
                + ((Global) Objects.requireNonNull(getActivity()).getApplicationContext()).getUser().getNickname()
                + "!";
        textViewWelcome.setText(welcomeMsg);
        drawMainEvent();
        drawFriends();
    }

    private void drawMainEvent() {
        // Sets data from upcoming event into the view
        List<Event> events = ((Global) this.getActivity().getApplicationContext()).getUpcoming();

        if (events.size() == 0) noUpcoming();
        else {
            events.sort((o1, o2) -> o1.getFromDate().compareTo(o2.getFromDate()));
            Event event = events.get(0);

            textViewBuddies.setText(this.getActivity().getResources().getString(R.string.buddies));
            String date = parseDate(event.getFromDate().toString()) + " - " + parseDate(event.getToDate().toString());
            String location = event.getDestination().getCountry().getName() + ", " + event.getDestination().getCity();
            textViewDate.setText(date);
            textViewLocation.setText(location);
            Picasso.get().load(event.getAuthor().getPicture())
                    .resize(100, 100).into(imageViewCreator);
            if (event.getParticipants().size() > 0)
                Picasso.get().load(event.getParticipants().get(0).getPicture())
                        .resize(100, 100).into(imageViewBuddy1);
            if (event.getParticipants().size() > 1)
                Picasso.get().load(event.getParticipants().get(1).getPicture())
                        .resize(100, 100).into(imageViewBuddy2);
        }
    }

    private String parseDate(String date) {
        return date.substring(0, 4) + date.substring(8, 10) + ". " + date.substring(4, 7) + date.substring(date.indexOf("G") + 9);
    }

    private void noUpcoming() {
        String msgTop = "No adventures coming up...";
        String msgBot = "Let's change that!";
        textViewDate.setText(msgBot);
        textViewLocation.setText(msgTop);
    }

    private void drawFriends() {
        // Gets a list of friends from the Global context
        List<User> friends = ((Global) this.getActivity().getApplicationContext()).getFriendsOfCurrentUser();
        if (friends == null) return;

        // for each friend creates a View with ImageView and TextView inside
        for (User friend : friends) {
            linearLayoutBuddies.addView(factory.getFriendContainer(friend.getPicture()
                    , friend
                    , coordinatorLayout
                    , friend.getId()
            ));
        }
    }
}
