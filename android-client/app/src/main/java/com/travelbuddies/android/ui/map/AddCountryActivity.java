package com.travelbuddies.android.ui.map;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.travelbuddies.android.MainActivity;
import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.Country;
import com.travelbuddies.android.backendBridge.providers.UserProvider;
import com.travelbuddies.android.utils.Global;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddCountryActivity extends AppCompatActivity {

    private UserProvider userProvider;
    private AutoCompleteTextView addCountryACTV;
    private List<String> countries = new ArrayList<>();
    private List<Country> cCountries = new ArrayList<>();
    private ImageButton addCountryBtn;
    private FloatingActionButton floatingActionButtonBackFromAddCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_country);
        userProvider = new UserProvider(getApplicationContext());
        addCountryACTV = findViewById(R.id.addCountryACTV);
        addCountryBtn = findViewById(R.id.addCountryBtn);
        floatingActionButtonBackFromAddCountry = findViewById(R.id.floatingActionButtonBackFromAddCountry);

        addCountryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Optional<Country> country = getCountryId();
                if (!country.isPresent()) return;
                userProvider.addVisitedCountry(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Log.i("AddCountryActivity", "Country added.");
                        Toast.makeText(getApplicationContext(), "Country added.", Toast.LENGTH_SHORT).show();
                        Country c = cCountries.stream().filter(x -> x.getName().equals(addCountryACTV.getText().toString())).findAny().get();
                        ((Global) getApplicationContext()).getVisitedCountries().add(c);
                        addCountryACTV.setText("");
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("AddCountryActivity", "Failed to add country.");
                    }
                }, country.get().getId()    );
            }
        });

        userProvider.getAllCountries(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
                cCountries.addAll(response.body());
                countries.addAll(response.body().stream().map(Country::getName).collect(Collectors.toList()));
                setupAutofill();
                Log.i("AddCountryActivity", "Received countries from server.");
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {
                Log.e("AddCountryActivity", "Failed to get countries from server.");
            }
        });

        floatingActionButtonBackFromAddCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class).putExtra("fragment", "map"));

            }
        });
    }

    private void setupAutofill(){
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, countries);
        arrayAdapter.setNotifyOnChange(true);
        addCountryACTV.setThreshold(1);
        addCountryACTV.setAdapter(arrayAdapter);
    }

    private Optional<Country> getCountryId(){
        Optional<Country> country = cCountries.stream().filter(x -> x.getName().equals(addCountryACTV.getText().toString())).findAny();
        return country;
    }
}