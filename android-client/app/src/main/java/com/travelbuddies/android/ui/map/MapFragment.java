package com.travelbuddies.android.ui.map;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.Country;
import com.travelbuddies.android.factory.MapFactory;
import com.travelbuddies.android.utils.Global;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


public class MapFragment extends Fragment {

    private LinearLayout linearLayoutVisitedCountries;
    private MapFactory factory;
    private Button addCountry;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.map_fragment, container, false);
        factory = new MapFactory(getContext());

        linearLayoutVisitedCountries = root.findViewById(R.id.linearLayoutVisitedCountries);
        addCountry = root.findViewById(R.id.addCountry);

        addCountry.setOnClickListener(v -> startActivity(new Intent(getActivity().getApplicationContext(), AddCountryActivity.class)));

        drawVisitedCountries();

        return root;
    }

    private void drawVisitedCountries() {
        List<Country> countries = ((Global) Objects.requireNonNull(this.getActivity()).getApplicationContext()).getVisitedCountries();

        if (countries.size() == 0)
            linearLayoutVisitedCountries.addView(factory.getVisitedCountryContainer("", "No visited countries.", true));

        List<Country> countryList = new ArrayList<>();
        for (Country country : countries) {
            if (countryList.contains(country)) continue;
            countryList.add(country);
            linearLayoutVisitedCountries.addView(factory.getVisitedCountryContainer(country.getAbbreviation(), country.getName(), false));
        }
    }
}
