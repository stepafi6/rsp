package com.travelbuddies.android.backendBridge.model;

import java.sql.Timestamp;

public class Message {

    long id;
    private String content;
    private Timestamp timestamp;

    public User systemUser;

    public Message(long id, String content, Timestamp timestamp, User user) {
        this.id = id;
        this.content = content;
        this.timestamp = timestamp;
        this.systemUser = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public User getUser() {
        return systemUser;
    }

    public void setUser(User user) {
        this.systemUser = user;
    }
}
