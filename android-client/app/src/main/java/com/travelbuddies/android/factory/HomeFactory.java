package com.travelbuddies.android.factory;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.travelbuddies.android.MainActivity;
import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.User;
import com.travelbuddies.android.backendBridge.providers.UserProvider;
import com.travelbuddies.android.utils.Global;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFactory extends ChiefFactory {

    public HomeFactory(Context context) {
        super(context);
    }

    public View getFriendContainer(String picture, User user, CoordinatorLayout coordinatorLayout, String id) {
        // One by one targeting prepared layouts
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.friend_container, null);
        ImageButton imageButton = (ImageButton) LayoutInflater.from(context).inflate(R.layout.profile_photo, null);
        imageButton.setBackgroundColor(Color.TRANSPARENT);
        TextView textView = (TextView) LayoutInflater.from(context).inflate(R.layout.nickname, null);

        imageButton.setOnClickListener(v -> Snackbar.make(coordinatorLayout, "Delete friend?", Snackbar.LENGTH_LONG)
                .setAction("Delete", v1 -> {
                    Snackbar.make(coordinatorLayout, "deleting...", Snackbar.LENGTH_SHORT);
                    UserProvider userProvider = new UserProvider(context);
                    userProvider.deleteFriend(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            Log.i("HomeFactory", "User deleted.");
                            Toast.makeText(context, "Friend deleted", Toast.LENGTH_SHORT).show();
                            ((Global) context).getFriendsOfCurrentUser().remove(user);
                            context.startActivity(new Intent(context, MainActivity.class));
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Log.i("HomeFactory", "Something went wrong, user was not deleted.");
                        }
                    }, id);
                }).show());

        // Sets data into the views
        Picasso.get().load(picture)
                .resize(100, 100).into(imageButton);
        textView.setText(user.getNickname());
        linearLayout.addView(imageButton);
        linearLayout.addView(textView);

        return linearLayout;
    }
}
