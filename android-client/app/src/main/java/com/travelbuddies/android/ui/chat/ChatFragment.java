package com.travelbuddies.android.ui.chat;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;

import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.Conversation;
import com.travelbuddies.android.backendBridge.model.User;
import com.travelbuddies.android.factory.ChatFactory;
import com.travelbuddies.android.utils.Global;

import java.util.List;
import java.util.stream.Collectors;

public class ChatFragment extends Fragment {

    private LinearLayout container;
    private ChatFactory factory;
    private CoordinatorLayout coordinatorLayout;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.chat_fragment, container, false);
        this.container = root.findViewById(R.id.container);
        factory = new ChatFactory(this.getActivity().getApplicationContext());
        List<Conversation> conversations = ((Global) getActivity().getApplicationContext()).getConversations();
        coordinatorLayout = root.findViewById(R.id.banner);

        for (Conversation c : conversations) {
            List<User> usersWithoutMe = c.systemUsers.stream().filter(x -> !x.equals(((Global) getActivity().getApplicationContext()).getUser())).collect(Collectors.toList());
            if (usersWithoutMe.isEmpty()) return root;

            StringBuilder names = new StringBuilder();
            int i = 0;
            for (User user : usersWithoutMe) {
                if (i < 3) {
                    names.append(user.getNickname());
                    names.append(", ");
                }
                i++;
            }
            names.delete(names.length() - 2, names.length() - 1);
            this.container.addView(factory.getChatContainer(usersWithoutMe.get(0).getPicture(), names.toString(), !c.messages.isEmpty() ? c.messages.get(0).getContent() : "", coordinatorLayout, c.getId()));
        }
        return root;
    }
}
