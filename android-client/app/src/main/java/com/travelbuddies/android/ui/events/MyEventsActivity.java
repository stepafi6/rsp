package com.travelbuddies.android.ui.events;

import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.travelbuddies.android.MainActivity;
import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.Event;
import com.travelbuddies.android.factory.EventsFactory;
import com.travelbuddies.android.utils.Global;

import java.util.Date;
import java.util.List;

public class MyEventsActivity extends AppCompatActivity {

    private LinearLayout linearLayout;
    private FloatingActionButton buttonBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_events);

        linearLayout = findViewById(R.id.linearLayoutEventsContainer);
        buttonBack = findViewById(R.id.backFromMyEvents);

        EventsFactory eventsFactory = new EventsFactory(getApplicationContext());
        List<Event> events = ((Global) getApplicationContext()).getMyEvents();

        events.sort((x, y) -> (x.getFromDate().compareTo(y.getFromDate())));
        events.removeIf(x -> (x.getFromDate().before(new Date())));

        for (Event event : events) {
            String budy1 = event.getParticipants().size() > 0 ? event.getParticipants().get(0).getPicture() : "";
            String budy2 = event.getParticipants().size() > 1 ? event.getParticipants().get(1).getPicture() : "";
            String date = parseDate(event.getFromDate().toString()) + " - " + parseDate(event.getToDate().toString());
            String where = event.getDestination().getCountry().getName() + ", " + event.getDestination().getCity();
            linearLayout.addView(eventsFactory.getEventView(date, where, event.getAuthor().getPicture(), budy1, budy2));
        }

        buttonBack.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), MainActivity.class)));
    }

    private String parseDate(String date) {
        return date.substring(0, 4) + date.substring(8, 10) + ". " + date.substring(4, 7) + date.substring(date.indexOf("G") + 9);
    }
}
