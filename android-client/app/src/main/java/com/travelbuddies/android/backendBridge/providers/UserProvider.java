package com.travelbuddies.android.backendBridge.providers;

import android.content.Context;

import com.travelbuddies.android.backendBridge.model.Country;
import com.travelbuddies.android.backendBridge.model.User;
import com.travelbuddies.android.backendBridge.services.UserService;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class UserProvider extends AbstractProvider<UserService> {

    public UserProvider(Context context) {
        super(context, UserService.class);
    }

    public void getCurrent(Callback<User> callback) {
        Call<User> call = service.getCurrent();

        //Perform request
        call.enqueue(callback);
    }

    public void searchUsers(Callback<List<User>> callback, String query) {
        Call<List<User>> call = service.searchUsers(query);

        //Perform request
        call.enqueue(callback);
    }

    public void getFriendsOfCurrent(Callback<List<User>> callback) {
        Call<List<User>> call = service.getFriendsOfCurrent();

        //Perform request
        call.enqueue(callback);
    }

    public void getVisitedCountriesOfCurrent(Callback<List<Country>> callback) {
        Call<List<Country>> call = service.getVisitedCountriesOfCurrent();

        //Perform request
        call.enqueue(callback);
    }

    public void addFriend(Callback<User> callback, String userId) {
        Call<User> call = service.addFriend(userId);

        //Perform request
        call.enqueue(callback);
    }

    public void addVisitedCountry(Callback<ResponseBody> callback, long id) {
        Call<ResponseBody> call = service.addVisitedCountry(id);

        //Perform request
        call.enqueue(callback);
    }

    public void deleteFriend(Callback<ResponseBody> callback, String id) {
        Call<ResponseBody> call = service.deleteFriend(id);

        //Perform request
        call.enqueue(callback);
    }

    public void getAllCountries(Callback<List<Country>> callback){
        Call<List<Country>> call = service.getAllCountries();

        call.enqueue(callback);
    }
}
