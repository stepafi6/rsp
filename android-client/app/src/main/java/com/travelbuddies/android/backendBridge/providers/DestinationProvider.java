package com.travelbuddies.android.backendBridge.providers;

import android.content.Context;

import com.travelbuddies.android.backendBridge.model.Destination;
import com.travelbuddies.android.backendBridge.services.DestinationService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


public class DestinationProvider extends AbstractProvider<DestinationService> {

    public DestinationProvider(Context context) {
        super(context, DestinationService.class);
    }

    public void suggestCities(Callback<List<Destination>> callback, String name) {
        Call<List<Destination>> call = service.suggestCities(name);

        //Perform request
        call.enqueue(callback);
    }

    public void suggestSubCountries(Callback<List<Destination>> callback, String name) {
        Call<List<Destination>> call = service.suggestSubCountries(name);

        //Perform request
        call.enqueue(callback);
    }

    public void suggestDestinations(Callback<List<Destination>> callback, String name) {
        Call<List<Destination>> call = service.suggestDestinations(name);

        //Perform request
        call.enqueue(callback);
    }


}
