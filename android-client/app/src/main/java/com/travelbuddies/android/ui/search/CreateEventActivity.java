package com.travelbuddies.android.ui.search;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.travelbuddies.android.LoadingActivity;
import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.Destination;
import com.travelbuddies.android.backendBridge.model.Event;
import com.travelbuddies.android.backendBridge.model.User;
import com.travelbuddies.android.backendBridge.providers.DestinationProvider;
import com.travelbuddies.android.backendBridge.providers.EventProvider;
import com.travelbuddies.android.utils.Global;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class CreateEventActivity extends AppCompatActivity {

    private ImageButton imageButtonBackFromCreateEvent;
    private TextView textViewDateFrom;
    private TextView textViewDateTo;
    private Activity activity;
    private Button buttonCreateEventSubmit;
    private EditText editTextDescription;

    private AutoCompleteTextView editTextWhereCreateEvent;
    private DestinationProvider destinationProvider;
    private List<Destination> hints;
    private List<String> strings = new ArrayList<>();
    private ArrayAdapter arrayAdapter;

    private DatePickerDialog.OnDateSetListener DateSetListenerFrom;
    private DatePickerDialog.OnDateSetListener DateSetListenerTo;

    // There needs to be a default value to recognise differences
    private String string = "init";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
        activity = this;
        EventProvider eventProvider = new EventProvider(getApplicationContext());
        destinationProvider = new DestinationProvider(getApplicationContext());

        imageButtonBackFromCreateEvent = findViewById(R.id.imageButtonBackFromCreateEvent);
        textViewDateFrom = findViewById(R.id.textViewDateFrom);
        textViewDateTo = findViewById(R.id.textViewDateTo);
        buttonCreateEventSubmit = findViewById(R.id.buttonCreateEventSubmit);
        editTextDescription = findViewById(R.id.editTextDescription);
        editTextWhereCreateEvent = findViewById(R.id.editTextWhereCreateEvent);

        imageButtonBackFromCreateEvent.setOnClickListener(v -> onBackPressed());

        String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());

        textViewDateFrom.setText(date);
        textViewDateTo.setText(date);

        textViewDateFrom.setOnClickListener(view -> {
            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(
                    activity,
                    R.style.DialogTheme,
                    DateSetListenerFrom,
                    year, month, day);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
            dialog.show();
        });

        textViewDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        activity,
                        R.style.DialogTheme,
                        DateSetListenerTo,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.show();
            }
        });

        DateSetListenerFrom = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + year + "-" + month + "-" + day);

                String date = year + "-" + month + "-" + day;
                textViewDateFrom.setText(date);
            }
        };

        DateSetListenerTo = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + year + "-" + month + "-" + day);

                String date = year + "-" + month + "-" + day;
                textViewDateTo.setText(date);
            }
        };

        buttonCreateEventSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Trying to create an event...");
                Event event = null;
                Destination destination = null;

                if (hints == null) return;

                for (Destination d : hints) {
                    if (d.getCity().equals(editTextWhereCreateEvent.getText().toString()))
                        destination = d;
                }

                if (destination == null) return;

                try {
                    List<User> p = new ArrayList<User>();
                    p.add(((Global) getApplicationContext()).getUser());
                    event = new Event(editTextWhereCreateEvent.getText().toString(), new SimpleDateFormat("yyyy-MM-dd").parse(textViewDateFrom.getText().toString()), new SimpleDateFormat("yyyy-MM-dd").parse(textViewDateTo.getText().toString()), destination, ((Global) getApplicationContext()).getUser(), p, false);
                    System.out.println("Event details:\t" + event);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                eventProvider.createEvent(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        System.out.println("Event created.");
                        System.out.println(response.body());
                        startActivity(new Intent(getApplicationContext(), LoadingActivity.class));
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        System.out.println("Event could not be created.");
                    }
                }, event);
            }
        });

        arrayAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, strings);
        arrayAdapter.setNotifyOnChange(true);
        editTextWhereCreateEvent.setThreshold(1);
        editTextWhereCreateEvent.setAdapter(arrayAdapter);

        editTextWhereCreateEvent.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                String string2 = editTextWhereCreateEvent.getText().toString();
                if (string.equals(string2)) return;
                string = string2;
                editTextWhereCreateEvent.dismissDropDown();
                if (s.length() != 0) {
                    hintMeResults(editTextWhereCreateEvent.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void hintMeResults(String text) {
        destinationProvider.suggestDestinations(new Callback<List<Destination>>() {
            @Override
            public void onResponse(Call<List<Destination>> call, Response<List<Destination>> response) {
                hints = response.body();
                if (hints == null) return;
                List<String> autoFill = hints.stream().map(Destination::getCity).collect(Collectors.toList());

                // to ensure autofill is using correct values, we have to for some reason force him to drop data and
                // manually reload the input
                arrayAdapter.clear();
                arrayAdapter.addAll(autoFill);
                forceReload();
                editTextWhereCreateEvent.showDropDown();
            }

            @Override
            public void onFailure(Call<List<Destination>> call, Throwable t) {
                Log.i("CreateEventActivity", "Can not get data");
            }
        }, text);
    }

    private void forceReload() {
        editTextWhereCreateEvent.setText(editTextWhereCreateEvent.getText().toString());
        editTextWhereCreateEvent.setSelection(editTextWhereCreateEvent.getText().toString().length());
    }
}
