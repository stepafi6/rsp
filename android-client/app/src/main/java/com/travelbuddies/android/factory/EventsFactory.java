package com.travelbuddies.android.factory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.travelbuddies.android.R;

public class EventsFactory extends ChiefFactory {

    public EventsFactory(Context context) {
        super(context);
    }

    public View getEventView(String when, String where, String crea, String bud, String bud2) {
        // Master init
        LinearLayout event = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.my_event, null);

        // Children targeting
        LinearLayout linearLayout0 = ((LinearLayout) event.getChildAt(0));
        LinearLayout linearLayout1 = ((LinearLayout) event.getChildAt(1));
        LinearLayout linearLayout3 = ((LinearLayout) event.getChildAt(3));

        // values setting
        ((TextView) linearLayout0.getChildAt(0)).setText(where);
        ((TextView) linearLayout1.getChildAt(0)).setText(when);

        Picasso.get().load(crea).resize(60, 60).into((ImageView) linearLayout3.getChildAt(0));
        if (!bud.equals(""))
            Picasso.get().load(bud).resize(50, 50).into((ImageView) linearLayout3.getChildAt(2));
        if (!bud2.equals(""))
            Picasso.get().load(bud2).resize(60, 60).into((ImageView) linearLayout3.getChildAt(3));

        return event;
    }
}
