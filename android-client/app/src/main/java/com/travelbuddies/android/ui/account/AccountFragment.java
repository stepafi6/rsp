package com.travelbuddies.android.ui.account;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;
import com.travelbuddies.android.LoginActivity;
import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.User;
import com.travelbuddies.android.utils.Global;

import java.util.Objects;

public class AccountFragment extends Fragment {

    private ImageView imageViewProfilePhoto;
    private TextView textViewUsername;
    private TextView textViewUserEmail;
    private TextView textViewBuddiesAmount;
    private TextView textViewTripsAmount;
    private Button buttonLogout;
    private User user;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.account_fragment, container, false);
        user = ((Global) Objects.requireNonNull(this.getActivity()).getApplicationContext()).getUser();

        // Getting data from the state holder
        int tripsAmount = ((Global) this.getActivity().getApplicationContext()).getMyEvents().size();
        int numberOfFriends = ((Global) this.getActivity().getApplicationContext()).getFriendsOfCurrentUser().size();
        String trips = "Trips " + tripsAmount;
        String buddies = "Buddies " + numberOfFriends;

        imageViewProfilePhoto = root.findViewById(R.id.imageViewProfilePhoto);
        textViewBuddiesAmount = root.findViewById(R.id.textViewBuddiesAmount);
        textViewTripsAmount = root.findViewById(R.id.textViewTripsAmount);
        textViewUsername = root.findViewById(R.id.textViewUsername);
        textViewUserEmail = root.findViewById(R.id.textViewUserEmail);
        buttonLogout = root.findViewById(R.id.buttonLogout);

        textViewTripsAmount.setText(trips);
        textViewBuddiesAmount.setText(buddies);
        textViewUsername.setText(user.getNickname());
        textViewUserEmail.setText(user.getEmail());

        // Profile pic load
        Picasso.get().load(((Global) this.getActivity().getApplicationContext()).getUser().getPicture())
                .resize(400, 400).into(imageViewProfilePhoto);

        buttonLogout.setOnClickListener(v -> logout());

        return root;
    }

    private void logout() {
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.putExtra(LoginActivity.EXTRA_CLEAR_CREDENTIALS, true);
        startActivity(intent);
    }
}
