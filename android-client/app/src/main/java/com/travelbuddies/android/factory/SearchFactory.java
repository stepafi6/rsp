package com.travelbuddies.android.factory;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.Event;
import com.travelbuddies.android.backendBridge.providers.EventProvider;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFactory extends ChiefFactory {

    public SearchFactory(Context context) {
        super(context);
    }

    public View getEvent(String when, String where, String crea, String bud, String bud2, long id) {

        // Initializing master
        LinearLayout event = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.event, null);

        // Targeting children
        View view = event.getChildAt(2);
        ((LinearLayout) event.getChildAt(3)).getChildAt(4).setBackgroundColor(Color.TRANSPARENT);
        ((LinearLayout) event.getChildAt(3)).getChildAt(4).setOnClickListener(v -> {
            Snackbar.make(view, "Want to join the event?", Snackbar.LENGTH_LONG)
                    .setAction("Yes", v1 -> {
                        EventProvider eventProvider = new EventProvider(context);
                        eventProvider.attendEvent(new Callback<Event>() {
                            @Override
                            public void onResponse(Call<Event> call, Response<Event> response) {
                                Log.i("SearchFactory", "user added to the event");
                                Toast.makeText(context, "Joined the event", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(Call<Event> call, Throwable t) {
                                Log.e("SearchFactory", "failed to add user to the event");
                            }
                        }, id);
                    }).show();
        });

        // set data
        ((TextView) event.getChildAt(0)).setText(where);
        ((TextView) event.getChildAt(1)).setText(when);
        Picasso.get().load(crea).resize(60, 60)
                .into(((ImageView) ((LinearLayout) event.getChildAt(3)).getChildAt(0)));
        if (!bud.equals("")) Picasso.get().load(bud).resize(50, 50)
                .into(((ImageView) ((LinearLayout) event.getChildAt(3)).getChildAt(2)));
        if (!bud2.equals("")) Picasso.get().load(bud2).resize(50, 50)
                .into(((ImageView) ((LinearLayout) event.getChildAt(3)).getChildAt(3)));

        return event;
    }
}
