package com.travelbuddies.android.backendBridge.services;

import com.travelbuddies.android.backendBridge.model.Event;
import com.travelbuddies.android.backendBridge.model.SearchEventRequestBody;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface EventService {

    @GET("/api/events/")
    Call<List<Event>> getEvents();

    @GET("/api/events/my")
    Call<List<Event>> getMyEvents();

    @GET("/api/events/{id}")
    Call<Event> getEvent(@Path("id") Integer id);

    @DELETE("/api/events/{id}")
    Call<ResponseBody> deleteEvent(@Path("id") Integer id);

    @POST("/api/events/")
    Call<ResponseBody> createEvent(@Body Event event);

    @PUT("/api/events/")
    Call<Event> updateEvent(@Body Event event);

    @POST("/api/events/search")
    Call<List<Event>> getEventsFromSearch(@Body SearchEventRequestBody searchRequest);

    @GET("/api/events/upcoming")
    Call<List<Event>> getUpcomingEvent();

    @GET("/api/events/attend/{id}")
    Call<Event> attendEvent(@Path("id") Long id);
}
