package com.travelbuddies.android.ui.search;

import android.os.Bundle;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.Event;
import com.travelbuddies.android.factory.SearchFactory;
import com.travelbuddies.android.utils.Global;

import java.util.List;

public class AfterSearchActivity extends AppCompatActivity {

    private FloatingActionButton floatingActionButton;
    private SearchFactory factory;
    private LinearLayout wrapper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_search);

        factory = new SearchFactory(getApplicationContext());
        floatingActionButton = findViewById(R.id.floatingActionButton);
        wrapper = findViewById(R.id.wrapper);

        floatingActionButton.setOnClickListener(v -> onBackPressed());

        List<Event> events = ((Global) getApplicationContext()).getSearchEvents();

        // Renders search events to the user
        for (Event e : events) {
            if (e.getAuthor() == null || e.getDestination() == null || e.getFromDate() == null || e.getToDate() == null)
                continue;
            String when = parseDate(e.getFromDate().toString()) + " - " + parseDate(e.getToDate().toString());
            String where = e.getDestination().getCountry().getName() + ", " + e.getDestination().getCity();
            String creator = e.getAuthor().getPicture();
            String bud1 = "";
            String bud2 = "";
            if (e.getParticipants().size() > 0) bud1 = e.getParticipants().get(0).getPicture();
            if (e.getParticipants().size() > 1) bud2 = e.getParticipants().get(1).getPicture();
            wrapper.addView(factory.getEvent(when, where, creator, bud1, bud2, e.getId()));
        }

    }

    private String parseDate(String date) {
        return date.substring(0, 4) + date.substring(8, 10) + ". " + date.substring(4, 7) + date.substring(date.indexOf("G") + 9);
    }
}
