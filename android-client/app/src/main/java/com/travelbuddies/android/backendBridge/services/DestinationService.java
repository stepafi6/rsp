package com.travelbuddies.android.backendBridge.services;

import com.travelbuddies.android.backendBridge.model.Destination;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface DestinationService {

    @GET("/api/destinations/suggest/cities/{name}")
    Call<List<Destination>> suggestCities(@Path("name") String name);

    @GET("/api/destinations/suggest/sub-countries/{name}")
    Call<List<Destination>> suggestSubCountries(@Path("name") String name);

    @GET("/api/destinations/suggest/destinations/{name}")
    Call<List<Destination>> suggestDestinations(@Path("name") String name);

}
