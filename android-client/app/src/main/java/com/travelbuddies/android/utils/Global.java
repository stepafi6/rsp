package com.travelbuddies.android.utils;

import android.app.Application;

import com.travelbuddies.android.backendBridge.model.Conversation;
import com.travelbuddies.android.backendBridge.model.Country;
import com.travelbuddies.android.backendBridge.model.Event;
import com.travelbuddies.android.backendBridge.model.Message;
import com.travelbuddies.android.backendBridge.model.User;

import java.util.List;

/**
 * Main state holder
 */
public class Global extends Application {


    private User user;
    private List<Event> upcoming;
    private List<User> friendsOfCurrentUser;
    private List<Country> visitedCountries;
    private List<Conversation> conversations;
    private List<Message> messages;
    private List<Event> searchEvents;
    private List<Event> myEvents;

    public List<Event> getMyEvents() {
        return myEvents;
    }

    public void setMyEvents(List<Event> myEvents) {
        this.myEvents = myEvents;
    }

    public List<Event> getSearchEvents() {
        return searchEvents;
    }

    public void setSearchEvents(List<Event> searchEvents) {
        this.searchEvents = searchEvents;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public List<Conversation> getConversations() {
        return conversations;
    }

    public void setConversations(List<Conversation> conversations) {
        this.conversations = conversations;
    }

    public List<Country> getVisitedCountries() {
        return visitedCountries;
    }

    public void setVisitedCountries(List<Country> visitedCountries) {
        this.visitedCountries = visitedCountries;
    }

    public List<Event> getUpcoming() {
        return upcoming;
    }

    public void setUpcoming(List<Event> upcoming) {
        this.upcoming = upcoming;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getFriendsOfCurrentUser() {
        return friendsOfCurrentUser;
    }

    public void setFriendsOfCurrentUser(List<User> friendsOfCurrentUser) {
        this.friendsOfCurrentUser = friendsOfCurrentUser;
    }
}
