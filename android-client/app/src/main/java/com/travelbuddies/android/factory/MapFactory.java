package com.travelbuddies.android.factory;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.travelbuddies.android.R;

public class MapFactory extends ChiefFactory {

    public MapFactory(Context context) {
        super(context);
    }

    public View getVisitedCountryContainer(String abb, String country, boolean empty) {
        // One by one targeting prepared layouts
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.visited_country_container, null);
        ImageView imageView = (ImageView) LayoutInflater.from(context).inflate(R.layout.flag_container, null);
        TextView textView = (TextView) LayoutInflater.from(context).inflate(R.layout.country_name, null);
        View spacer = (View) LayoutInflater.from(context).inflate(R.layout.spacer, null);

        // Sets data into the views
        if (!empty) {
            String path = "com.travelbuddies.android:drawable/" + abb.toLowerCase() + "_svg";
            int resID = context.getResources().getIdentifier(path, null, null);
            Drawable drawable = context.getDrawable(resID);

            imageView.setImageDrawable(drawable);
        }

        textView.setText(country);
        linearLayout.addView(imageView);
        linearLayout.addView(textView);
        linearLayout.addView(spacer);

        return linearLayout;
    }
}
