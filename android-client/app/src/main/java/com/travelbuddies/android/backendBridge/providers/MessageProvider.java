package com.travelbuddies.android.backendBridge.providers;

import android.content.Context;

import com.travelbuddies.android.backendBridge.model.Conversation;
import com.travelbuddies.android.backendBridge.model.Event;
import com.travelbuddies.android.backendBridge.model.Message;
import com.travelbuddies.android.backendBridge.model.User;
import com.travelbuddies.android.backendBridge.services.MessageService;

import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;

public class MessageProvider extends AbstractProvider<MessageService> {


    public MessageProvider(Context context) {
        super(context, MessageService.class);
    }

    public void getMessagesFromConversation(Callback<List<Message>> callback, long id){
        Call<List<Message>> call = service.getMessagesFromConversation(id);

        //Perform request
        call.enqueue(callback);
    }

    public void getConversationsOfCurrentUser(Callback<List<Conversation>> callback){
        Call<List<Conversation>> call = service.getConversationsOfCurrentUser();

        //Perform request
        call.enqueue(callback);
    }

    public void createConversation(Callback<ResponseBody> callback, List<User> users){
        Call<ResponseBody> call = service.createConversation(users);

        //Perform request
        call.enqueue(callback);
    }

    public void sendMessage(Callback<ResponseBody> callback, long id, String message){
        RequestBody body =
                RequestBody.create(MediaType.parse("text/plain"), message);

        Call<ResponseBody> call = service.sendMessage(id, body);


        //Perform request
        call.enqueue(callback);
    }
}
