package com.travelbuddies.android.backendBridge.model;

import java.util.List;

public class Conversation {

    long id;

    public List<User> systemUsers;

    public List<Message> messages;

    public Conversation(long id, List<User> systemUsers, List<Message> messages) {
        this.id = id;
        this.systemUsers = systemUsers;
        this.messages = messages;
    }

    public long getId() {
        return id;
    }

    public List<User> getSystemUsers() {
        return systemUsers;
    }

    public void setSystemUsers(List<User> systemUsers) {
        this.systemUsers = systemUsers;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }
}
