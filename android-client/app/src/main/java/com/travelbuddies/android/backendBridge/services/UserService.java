package com.travelbuddies.android.backendBridge.services;

import com.travelbuddies.android.backendBridge.model.Country;
import com.travelbuddies.android.backendBridge.model.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UserService {

    @GET("/api/user")
    Call<User> getCurrent();

    @Headers({"Content-Type: application/json"})
    @GET("/api/user/friends/search/{query}")
    Call<List<User>> searchUsers(@Path("query") String query);

    @GET("/api/user/friends")
    Call<List<User>> getFriendsOfCurrent();

    @GET("/api/user/countries")
    Call<List<Country>> getVisitedCountriesOfCurrent();

    @GET("/api/user/countries/notvisited")
    Call<List<Country>> getAllCountries();

    @PUT("/api/user/friends/{id}")
    Call<User> addFriend(@Path("id") String id);

    @Headers({"Content-Type: application/json"})
    @DELETE("/api/user/friends/{id}")
    Call<ResponseBody> deleteFriend(@Path("id") String id);

    @PUT("/api/user/countries/{countryId}")
    Call<ResponseBody> addVisitedCountry(@Path("countryId") long countryId);

}
