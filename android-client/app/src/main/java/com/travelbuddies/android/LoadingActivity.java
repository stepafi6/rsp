package com.travelbuddies.android;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.travelbuddies.android.backendBridge.model.Conversation;
import com.travelbuddies.android.backendBridge.model.Country;
import com.travelbuddies.android.backendBridge.model.Event;
import com.travelbuddies.android.backendBridge.model.User;
import com.travelbuddies.android.backendBridge.providers.EventProvider;
import com.travelbuddies.android.backendBridge.providers.MessageProvider;
import com.travelbuddies.android.backendBridge.providers.UserProvider;
import com.travelbuddies.android.utils.Global;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadingActivity extends AppCompatActivity {

    private ProgressBar progressBar;
    private int done = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        progressBar = findViewById(R.id.progressBar);
        progressBar.setProgress(20, true);
        // Providers init
        EventProvider eventProvider = new EventProvider(getApplicationContext());
        MessageProvider messageProvider = new MessageProvider(getApplicationContext());
        UserProvider userProvider = new UserProvider(getApplicationContext());


        eventProvider.getUpcomingEvent(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                if (response.body() == null) {
                    failed("bad return", response.code());
                    return;
                }
                ((Global) getApplicationContext()).setUpcoming(response.body());
                finishedTask("upcoming event went ok");
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                failed("Server call to upcoming event failed!", t);
            }
        });

        userProvider.getCurrent(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.body() == null) {
                    failed("bad return", response.code());
                    return;
                }
                ((Global) getApplicationContext()).setUser(response.body());
                finishedTask("got user");
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                failed("Server call to get current user failed!", t);
            }
        });

        userProvider.getFriendsOfCurrent(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                if (response.body() == null) {
                    failed("bad return", response.code());
                    return;
                }
                ((Global) getApplicationContext()).setFriendsOfCurrentUser(response.body());
                finishedTask("Got friends of user");
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                failed("Server call to get current user's friends failed!", t);
            }
        });

        userProvider.getVisitedCountriesOfCurrent(new Callback<List<Country>>() {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response) {
                if (response.body() == null) {
                    failed("bad return", response.code());
                    return;
                }
                ((Global) getApplicationContext()).setVisitedCountries(response.body());
                finishedTask("got visited countries of current");
            }

            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {
                failed("Server call to get current user's visited countries failed!", t);
            }
        });

        messageProvider.getConversationsOfCurrentUser(new Callback<List<Conversation>>() {
            @Override
            public void onResponse(Call<List<Conversation>> call, Response<List<Conversation>> response) {
                if (response.body() == null) {
                    failed("bad return", response.code());
                    return;
                }
                ((Global) getApplicationContext()).setConversations(response.body());
                finishedTask("got conversations of current");
            }

            @Override
            public void onFailure(Call<List<Conversation>> call, Throwable t) {
                failed("Server call to get current user's conversations failed!", t);
            }
        });

        eventProvider.getMyEvents(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                if (response.body() == null) {
                    failed("bad return", response.code());
                    return;
                }
                ((Global) getApplicationContext()).setMyEvents(response.body());
                finishedTask("got user's events");
            }

            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                failed("didn't get user's events", t);
            }
        });

        progressBar.setProgress(50, true);
    }

    /**
     * Call successful
     * @param msg to be logged
     */
    private void finishedTask(String msg) {
        Log.i("LoadingActivity", msg);
        if (done == 5) startActivity(new Intent(getApplicationContext(), MainActivity.class));
        else done++;
    }

    /**
     * Call failed
     * @param msg to be logged
     * @param code return status code
     */
    private void failed(String msg, int code) {
        Log.e("LoadingActivity", msg + ", code: " + code);
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
    }

    /**
     * Call returned error
     * @param msg to be logged
     * @param t error to be thrown
     */
    private void failed(String msg, Throwable t) {
        Log.e("LoadingActivity", msg + ", stackTrace: " + t.getStackTrace());
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
    }

}
