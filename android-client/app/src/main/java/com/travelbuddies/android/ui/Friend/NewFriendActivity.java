package com.travelbuddies.android.ui.Friend;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.travelbuddies.android.MainActivity;
import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.User;
import com.travelbuddies.android.backendBridge.providers.UserProvider;
import com.travelbuddies.android.factory.NewFriendFactory;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewFriendActivity extends AppCompatActivity {

    private ImageButton imageButtonBack;
    private EditText editTextUsername;
    private Button buttonFindUser;
    private LinearLayout linearLayout;
    private NewFriendFactory newFriendFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);

        // Changing the behavior of the keyboard
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        imageButtonBack = findViewById(R.id.imageButtonBack);
        editTextUsername = findViewById(R.id.editTextUsername);
        buttonFindUser = findViewById(R.id.buttonFindUser);
        linearLayout = findViewById(R.id.linearLayout);
        newFriendFactory = new NewFriendFactory(getApplicationContext());
        UserProvider userProvider = new UserProvider(getApplicationContext());

        imageButtonBack.setOnClickListener(v -> startActivity(new Intent(getApplicationContext(), MainActivity.class)));

        buttonFindUser.setOnClickListener(v -> {
            String query = editTextUsername.getText().toString();
            userProvider.searchUsers(new Callback<List<User>>() {
                @Override
                public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                    if (response.body() == null) return;
                    linearLayout.removeAllViews();
                    for (User user : response.body()) {
                        View view = newFriendFactory.getUserView(user.getPicture(), user);
                        linearLayout.addView(view);
                    }
                }

                @Override
                public void onFailure(Call<List<User>> call, Throwable t) {
                    Log.i("NewFriendActivity", "Cannot find user.");
                }
            }, query);
        });
    }
}
