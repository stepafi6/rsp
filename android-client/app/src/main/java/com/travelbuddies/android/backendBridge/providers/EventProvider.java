package com.travelbuddies.android.backendBridge.providers;

import android.content.Context;

import com.travelbuddies.android.backendBridge.model.Event;
import com.travelbuddies.android.backendBridge.model.SearchEventRequestBody;
import com.travelbuddies.android.backendBridge.services.EventService;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;


public class EventProvider extends AbstractProvider<EventService> {

    public EventProvider(Context context) {
        super(context, EventService.class);
    }

    public void getEvents(Callback<List<Event>> callback) {
        Call<List<Event>> call = service.getEvents();

        //Perform request
        call.enqueue(callback);
    }

    public void getMyEvents(Callback<List<Event>> callback) {
        Call<List<Event>> call = service.getMyEvents();

        //Perform request
        call.enqueue(callback);
    }

    public void getEvent(Callback<Event> callback, Integer id) {
        Call<Event> call = service.getEvent(id);

        //Perform request
        call.enqueue(callback);
    }

    public void getUpcomingEvent(Callback<List<Event>> callback) {
        Call<List<Event>> call = service.getUpcomingEvent();

        //Perform request
        call.enqueue(callback);
    }

    public void deleteEvent(Callback<ResponseBody> callback, Integer id) {
        Call<ResponseBody> call = service.deleteEvent(id);

        //Perform request
        call.enqueue(callback);
    }

    public void createEvent(Callback<ResponseBody> callback, Event event) {
        Call<ResponseBody> call = service.createEvent(event);

        //Perform request
        call.enqueue(callback);
    }

    public void updateEvent(Callback<Event> callback, Event event) {
        Call<Event> call = service.updateEvent(event);

        //Perform request
        call.enqueue(callback);
    }

    public void getEventsFromSearch(Callback<List<Event>> callback, SearchEventRequestBody searchRequest){
        Call<List<Event>> call = service.getEventsFromSearch(searchRequest);

        call.enqueue(callback);
    }

    public void attendEvent(Callback<Event> callback, Long id){
        Call<Event> call = service.attendEvent(id);

        call.enqueue(callback);
    }
}
