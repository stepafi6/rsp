package com.travelbuddies.android.factory;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.travelbuddies.android.R;
import com.travelbuddies.android.backendBridge.model.Message;
import com.travelbuddies.android.backendBridge.providers.MessageProvider;
import com.travelbuddies.android.ui.chat.ChatActivity;
import com.travelbuddies.android.utils.Global;

import java.sql.Timestamp;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChatFactory extends ChiefFactory {
    public ChatFactory(Context context) {
        super(context);
    }

    public View getChatContainer(String picture, String name, String msg, CoordinatorLayout banner, long id) {
        // Initializing the master layout
        LinearLayout linearLayoutt = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.chat_container, null);

        // Targeting children
        LinearLayout linearLayout = (LinearLayout) linearLayoutt.getChildAt(0);
        ImageView imageView = (ImageView) linearLayout.getChildAt(0);
        TextView username = (TextView) ((LinearLayout) linearLayout.getChildAt(1)).getChildAt(0);
        TextView message = (TextView) ((LinearLayout) linearLayout.getChildAt(1)).getChildAt(1);

        // image load
        Picasso.get().load(picture).resize(65, 65).into(imageView);

        // on click listener to get messages from conversation
        linearLayoutt.setOnClickListener(v -> {
            MessageProvider provider = new MessageProvider(context);
            provider.getMessagesFromConversation(new Callback<List<Message>>() {
                @Override
                public void onResponse(Call<List<Message>> call, Response<List<Message>> response) {
                    ((Global) context).setMessages(response.body());
                    context.startActivity(new Intent(context, ChatActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra("id", id).putExtra("name", name));
                }

                @Override
                public void onFailure(Call<List<Message>> call, Throwable t) {
                    Log.e("Chat factory", "Messages from conversation not received");
                    Snackbar.make(banner, "Did not get messages!", Snackbar.LENGTH_SHORT).show();
                }
            }, id);
        });

        username.setText(name);
        message.setText(msg);

        return linearLayoutt;
    }

    public View getMessageContainer(String picture, Timestamp time, String message) {
        // Initializing the master layout
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(context).inflate(R.layout.message, null);

        // Targeting children
        LinearLayout child = (LinearLayout) linearLayout.getChildAt(0);
        ImageView imageView = (ImageView) child.getChildAt(0);
        TextView timer = (TextView) child.getChildAt(1);
        TextView msg = (TextView) child.getChildAt(2);

        // Data set
        Picasso.get().load(picture).resize(50, 50).into(imageView);
        timer.setText(formatTime(time));
        msg.setText(message);

        return linearLayout;
    }


    private String formatTime(Timestamp time) {
        Timestamp later = new Timestamp(time.getTime() + (1000 * 60 * 60 * 2));
        String text = later.toString();
        text = text.substring(11, 16);

        return text;
    }
}
