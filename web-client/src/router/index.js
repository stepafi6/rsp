import Vue from "vue";
import Router from "vue-router";
import Home from "../views/Home.vue";
import Map from "../views/Map";
import AddVisitedCountry from "../views/AddVisitedCountry";
import Search from "../views/Search";
import Event from "../views/Event";
import NewEvent from "../views/NewEvent";
import NewChat from "../views/NewChat";
import Conversation from "../views/Conversation";
import Chat from "../views/Chat";
import AddFriend from "../views/AddFriend";
import Login from "../views/Login.vue";
import Profile from "../views/Profile.vue";

import {authGuard} from "../auth";

import {BootstrapVue, BootstrapVueIcons} from 'bootstrap-vue/dist/bootstrap-vue.esm';

import axios from 'axios';

import vSelect from 'vue-select'

window.axios = axios;

Vue.use(Router);
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

Vue.component('v-select', vSelect)

const router = new Router({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [
        {
            path: "/login",
            name: "login",
            component: Login
        },
        {
            path: "/",
            name: "home",
            component: Home,
            beforeEnter: authGuard
        },
        {
            path: "/add-friend",
            name: "add-friend",
            component: AddFriend,
            beforeEnter: authGuard
        },
        {
            path: "/chat",
            name: "chats",
            component: Chat,
            beforeEnter: authGuard
        },
        {
            path: "/chat/:conversationId",
            name: "chat",
            component: Conversation,
            beforeEnter: authGuard
        },
        {
            path: "/new-chat",
            name: "new-chat",
            component: NewChat,
            beforeEnter: authGuard
        },
        {
            path: "/search",
            name: "search",
            component: Search,
            beforeEnter: authGuard
        },
        {
            path: "/events/:eventId",
            name: "event",
            component: Event,
            beforeEnter: authGuard
        },
        {
            path: "/new-event",
            name: "new-event",
            component: NewEvent,
            beforeEnter: authGuard
        },
        {
            path: "/map",
            name: "map",
            component: Map,
            beforeEnter: authGuard
        },
        {
            path: "/add-visited-country",
            name: "add-visited-country",
            component: AddVisitedCountry,
            beforeEnter: authGuard
        },
        {
            path: "/profile",
            name: "profile",
            component: Profile,
            beforeEnter: authGuard
        }
    ]
});

export default router;
