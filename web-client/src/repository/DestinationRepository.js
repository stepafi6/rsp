import Repository, {getConfig} from "./Repository";

const resource = "/destinations";

export default {
    autocompleteDestination(query, accessToken){
      return Repository.get(`${resource}/suggest/cities/${query}`, getConfig(accessToken))
    },
}
