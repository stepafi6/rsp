import Repository, {getConfig} from "./Repository";

const resource = "/conversations";

export default {
    getConversations(accessToken) {
        return Repository.get(`${resource}`, getConfig(accessToken))
    },

    createConversation(users, accessToken) {
        return Repository.post(`${resource}`, users, getConfig(accessToken))
    },

    getConversation(conversationId, accessToken) {
        return Repository.get(`${resource}/${conversationId}`, getConfig(accessToken))
    },

    sendMessage(conversationId, message, accessToken) {
        return Repository.put(`${resource}/${conversationId}`, message, {
            headers: {
                "Authorization": `Bearer ${accessToken}`,
                'Content-Type': 'text/plain'
            }
        });
    },

}
