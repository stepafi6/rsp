import Repository, {getConfig} from "./Repository";

const resource = "/events";

export default {
    upcomingEvents(accessToken) {
        return Repository.get(`${resource}/upcoming`, getConfig(accessToken))
    },
    searchEvents(searchObject, accessToken) {
        return Repository.post(`${resource}/search`, searchObject, getConfig(accessToken))
    },
    getEvent(eventId, accessToken) {
        return Repository.get(`${resource}/${eventId}`, getConfig(accessToken))
    },
    createEvent(event, accessToken) {
        return Repository.post(`${resource}/`, event, getConfig(accessToken))
    },
    attendEvent(eventId, accessToken) {
        return Repository.get(`${resource}/attend/${eventId}`, getConfig(accessToken))
    }
}
