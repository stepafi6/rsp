import axios from "axios";

const baseDomain = "https://tb-server.stepanek.app";
const baseUrl = `${baseDomain}/api`;

export function getConfig(accessToken){
    return {
        headers: {
            "Authorization": `Bearer ${accessToken}`
        }
    }
}

export default axios.create({
    baseURL: baseUrl
});
