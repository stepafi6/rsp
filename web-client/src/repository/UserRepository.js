import Repository, {getConfig} from "./Repository";

const resource = "/user";

export default {
    getFriends(accessToken) {
        return Repository.get(`${resource}/friends`, getConfig(accessToken))
    },
    getVisitedCountries(accessToken) {
        return Repository.get(`${resource}/countries`, getConfig(accessToken))
    },
    getAllCountries(accessToken) {
        return Repository.get(`${resource}/countries/notvisited`, getConfig(accessToken))
    },
    addVisitedCountry(countryId, accessToken) {
        return Repository.put(`${resource}/countries/${countryId}`, null, getConfig(accessToken))
    },
    searchUsers(query, accessToken) {
        return Repository.get(`${resource}/friends/search/${query}`, getConfig(accessToken))
    },
    addFriend(userId, accessToken) {
        return Repository.put(`${resource}/friends/${userId}`, null, getConfig(accessToken))
    }

}
