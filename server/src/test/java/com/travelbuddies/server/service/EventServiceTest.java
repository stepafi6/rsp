package com.travelbuddies.server.service;

import com.travelbuddies.server.dao.EventDao;
import com.travelbuddies.server.model.Event;
import com.travelbuddies.server.model.SystemUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.junit.jupiter.api.Assertions.*;


class EventServiceTest {

    @Mock
    private EventDao eventDaoMock;

    @InjectMocks
    private EventService eventService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void addParticipant() {
        Event event = new Event();
        SystemUser user = new SystemUser();
        eventService.addParticipant(event, user);
        assertFalse(event.getParticipants().isEmpty());
    }

    @Test
    void removeParticipant() {
        Event event = new Event();
        SystemUser user = new SystemUser();
        eventService.addParticipant(event, user);
        assertFalse(event.getParticipants().isEmpty());
        eventService.removeParticipant(event, user);
        assertTrue(event.getParticipants().isEmpty());
    }
}