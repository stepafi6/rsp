package com.travelbuddies.server.service;

import com.travelbuddies.server.dao.ConversationDao;
import com.travelbuddies.server.model.Conversation;
import com.travelbuddies.server.model.Message;
import com.travelbuddies.server.model.SystemUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class MessageServiceTest {

    @Mock
    private ConversationDao conversationDaoMock;

    private MessageService sut;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.sut = new MessageService(conversationDaoMock);
    }

    @Test
    void sendMessageAddsMessageToConversation() {
        final Conversation c = new Conversation();
        final String s = "Lorem ipsum";
        final SystemUser u = new SystemUser();

        assertNull(c.getMessages());

        sut.sendMessage(s, c, u);

        assertTrue(c.getMessages().stream().map(Message::getContent).anyMatch(x -> x.equals(s)));
    }

    @Test
    void createConversationCreatesConversationWithSpecifiedUsers() {
        //todo
    }

    @Test
    void getUserConversations() {
        //todo
    }

    @Test
    void getConversationMessagesReturnsAllMessagesFromConversation() {
        //todo
    }
}
