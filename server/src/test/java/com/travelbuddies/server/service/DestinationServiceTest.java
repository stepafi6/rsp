package com.travelbuddies.server.service;

import com.travelbuddies.server.dao.DestinationDao;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


public class DestinationServiceTest {

    @Mock
    private DestinationDao destinationDaoMock;

    @InjectMocks
    private DestinationService destinationService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }


}
