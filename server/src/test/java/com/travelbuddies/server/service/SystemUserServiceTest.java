package com.travelbuddies.server.service;

import com.travelbuddies.server.dao.CountryDao;
import com.travelbuddies.server.dao.SystemUserDao;
import com.travelbuddies.server.model.Country;
import com.travelbuddies.server.model.SystemUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

class SystemUserServiceTest {

    @Mock
    private SystemUserDao systemUserDaoMock;

    @Mock
    private CountryDao countryDao;

    private SystemUserService sut;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        this.sut = new SystemUserService(systemUserDaoMock,countryDao);
    }

    @Test
    void addFriend() {
        SystemUser user = new SystemUser();
        SystemUser friendToBeAdded = new SystemUser();
        sut.addFriend(user,friendToBeAdded);
        assertFalse(user.getFriends().isEmpty());
        assertFalse(friendToBeAdded.getFriends().isEmpty());
    }

    @Test
    void deleteFriend() {
        SystemUser user = new SystemUser();
        SystemUser friend = new SystemUser();
        sut.addFriend(user,friend);
        sut.deleteFriend(user,friend);
        assertTrue(user.getFriends().isEmpty());
        assertTrue(friend.getFriends().isEmpty());
    }

    @Test
    void addVisitedCountry() {
        Country country = new Country();
        country.setId(1);
        country.setName("Klingon");
        when(countryDao.findById(anyLong())).thenReturn(java.util.Optional.of(country));

        SystemUser user = new SystemUser();

        sut.addVisitedCountry(user, country.getId());
        assertTrue(user.getVisitedCountries().contains(country));
    }
}