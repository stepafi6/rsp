package com.travelbuddies.server.controller;

import com.travelbuddies.server.model.Destination;
import com.travelbuddies.server.service.DestinationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/api/destinations", produces = MediaType.APPLICATION_JSON_VALUE)
public class DestinationController {

    private static final Logger LOG = LoggerFactory.getLogger(DestinationController.class);

    private final DestinationService destinationService;

    @Autowired
    public DestinationController(DestinationService destinationService) {
        this.destinationService = destinationService;
    }

    @GetMapping(value = "/suggest/cities/{name}")
    public ResponseEntity<List<Destination>> suggestCities(@PathVariable String name) {
        final List<Destination> destinations = destinationService.autocompleteDestinationCity(name);
        LOG.info("Suggestion request has been made with including parameters city = {}", name);

        return new ResponseEntity<>(destinations, HttpStatus.OK);
    }

    @GetMapping(value = "/suggest/sub-countries/{name}")
    public ResponseEntity<List<Destination>> suggestSubCountries(@PathVariable String name) {
        final List<Destination> destinations = destinationService.autocompleteDestinationSubCountry(name);
        LOG.info("Suggestion request has been made with including parameters sub-country = {}", name);

        return new ResponseEntity<>(destinations, HttpStatus.OK);
    }

    @GetMapping(value = "/suggest/destinations/{name}")
    public ResponseEntity<List<Destination>> suggestDestinations(@PathVariable String name) {
        final List<Destination> destinations = destinationService.autocompleteDestination(name);
        LOG.info("Suggestion request has been made with including parameters destination = {}", name);

        return new ResponseEntity<>(destinations, HttpStatus.OK);
    }
}

