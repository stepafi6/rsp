package com.travelbuddies.server.controller;

import com.travelbuddies.server.dao.CountryDao;
import com.travelbuddies.server.exception.NotFoundException;
import com.travelbuddies.server.model.Country;
import com.travelbuddies.server.model.SystemUser;
import com.travelbuddies.server.service.SystemUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@RestController
@RequestMapping("/api/user")
public class SystemUserController {
    private static final Logger LOG = LoggerFactory.getLogger(SystemUserController.class);

    @Autowired
    private CountryDao countryDao;

    private SystemUserService systemUserService;
    private ControllerUtilities utilities;

    @Autowired
    public SystemUserController(SystemUserService systemUserService, ControllerUtilities utilities) {
        this.systemUserService = systemUserService;
        this.utilities = utilities;
    }

    /**
     * Returns current system user.
     */
    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SystemUser> getCurrent() {
        return new ResponseEntity<>(utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication()), HttpStatus.OK);
    }

    /**
     * Returns friend list of current system user.
     */
    @GetMapping(value = "/friends", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<SystemUser>> getFriendsOfCurrent() {
        SystemUser currentUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());
        LOG.info("User with id {} is asking for all friends", currentUser.getId());

        return new ResponseEntity<>(currentUser.getFriends(), HttpStatus.OK);
    }

    /**
     * Adds friend to a current user friend list.
     *
     * @param friendId UserId to be added as a friend.
     * @throws NotFoundException
     */
    @PutMapping(value = "/friends/{friendId}")
    public ResponseEntity<SystemUser> addFriend(@PathVariable String friendId) throws NotFoundException {
        SystemUser currentUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());

        SystemUser friendToBeAdded = systemUserService.findById(friendId);

        systemUserService.addFriend(currentUser, friendToBeAdded);
        LOG.info("User with id {} is asking to add a friend {}", currentUser.getId(), friendToBeAdded);

        final HttpHeaders headers = ControllerUtilities.createLocationHeaderFromCurrentUri("/{id}", friendToBeAdded.getId());
        return new ResponseEntity<>(friendToBeAdded, headers, HttpStatus.OK);
    }

    /**
     * Deletes friend from a current user friend list.
     *
     * @param id of User to be deleted form friend list.
     * @throws NullPointerException
     */
    @DeleteMapping(value = "/friends/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SystemUser> deleteFriend(@PathVariable String id) throws NotFoundException {
        SystemUser currentUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());

        final SystemUser friendToBeDeleted = systemUserService.getSystemUser(id);
        LOG.info("User with id {} is asking to delete a friend {}", currentUser.getId(),friendToBeDeleted);
        Objects.requireNonNull(friendToBeDeleted, String.format("User with id %d was not found.", id));


        systemUserService.deleteFriend(currentUser, friendToBeDeleted);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * Search for a user
     */
    @GetMapping(value = "/friends/search/{query}")
    public ResponseEntity<List<SystemUser>> searchUsers(@PathVariable String query) {
        LOG.info("Searching for a users with query {}", query);
        return new ResponseEntity<>( systemUserService.searchUsers(query), HttpStatus.OK);
    }

    /**
     * Returns list of visited countries of current system user.
     */
    @GetMapping(value = "/countries", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Set<Country>> getVisitedCountriesOfCurrent() {
        SystemUser currentUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());
        LOG.info("User with id {} is asking for visited countries", currentUser.getId());

        return new ResponseEntity<>( currentUser.getVisitedCountries(), HttpStatus.OK);
    }

    /**
     * Returns list of visited countries of current system user.
     */
    @GetMapping(value = "/countries/notvisited", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Country>> getAllCountries() {
        SystemUser currentUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());
        LOG.info("User with id {} is asking for not visited countries", currentUser.getId());
        return new ResponseEntity<>(systemUserService.getNotVisitedCountries(currentUser.getVisitedCountries()), HttpStatus.OK);
    }

    /**
     * Adds country to a list of current user visited countries.
     *
     * @param countryId Country to be added as visited country.
     * @throws NotFoundException
     */
    @PutMapping(value = "/countries/{countryId}")
    public ResponseEntity<Country> addVisitedCountry(@PathVariable long countryId) throws NotFoundException {
        SystemUser currentUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());
        LOG.info("User with id {} is asking to add  country with id {} to visited ", currentUser.getId(), countryId);

        systemUserService.addVisitedCountry(currentUser, countryId);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}

