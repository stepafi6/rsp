package com.travelbuddies.server.controller;

import com.travelbuddies.server.model.SystemUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Handles requests to "/api" endpoints.
 *
 * @see com.auth0.example.security.SecurityConfig to see how these endpoints are protected.
 */
@RestController
@RequestMapping(path = "api", produces = MediaType.APPLICATION_JSON_VALUE)
public class APIController {

    ControllerUtilities utilities;

    @Autowired
    public APIController(ControllerUtilities utilities) {
        this.utilities= utilities;
    }

    @GetMapping(value = "/public")
    public String publicEndpoint() {
        return "All good. You DO NOT need to be authenticated to call /api/public.";
    }

    @GetMapping(value = "/private")
    public SystemUser privateEndpoint() {
        //Get caller authorization with some Spring magic
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        //Cast it to OAuth2 token
        JwtAuthenticationToken token = (JwtAuthenticationToken)auth;

        //Fetch systemUser model from token
        SystemUser systemUser = utilities.GetUser(token);

        return systemUser;

        //return new Message("All good. You can see this because you are Authenticated.");
    }

    @GetMapping(value = "/private-scoped")
    public String privateScopedEndpoint() {
        return "All good. You can see this because you are Authenticated with a Token granted the 'read:messages' scope";
    }
}