package com.travelbuddies.server.controller;

import com.travelbuddies.server.model.Conversation;
import com.travelbuddies.server.model.Message;
import com.travelbuddies.server.model.SystemUser;
import com.travelbuddies.server.service.MessageService;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/conversations")
public class MessageController {

    private final MessageService messageService;

    private final ControllerUtilities utilities;

    @Autowired
    public MessageController(MessageService messageService, ControllerUtilities utilities) {
        this.messageService = messageService;
        this.utilities = utilities;
    }

    /**
     * Creates new conversation with specified users.
     *
     * @param users List of users in the new conversation.
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createConversation(@RequestBody List<SystemUser> users) {
        SystemUser systemUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());

        if (users.stream().map(x -> x.getId()).noneMatch(s -> s.equals(systemUser.getId())))
            users.add(systemUser);

        messageService.createConversation(users);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    /**
     * Returns list of conversations of current user.
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Conversation>> getConversationsOfCurrentUser() {
        SystemUser systemUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());

        return new ResponseEntity<>(messageService.getUserConversations(systemUser), HttpStatus.OK);
    }

    /**
     * Returns list of messages of specified conversation.
     *
     * @param conversationId Id of conversation.
     * @return List of messages from conversation.
     */
    @GetMapping(value = "/{conversationId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Message>> getMessagesFromConversation(
            @PathVariable long conversationId
    ) throws NotFoundException {
        SystemUser systemUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());
        final Conversation conversation = messageService.getConversationById(conversationId);
        if (conversation == null) {
            throw new NotFoundException("Conversation with id " + conversationId + " has not been found.");
        }
        if (!conversation.getSystemUsers().contains(systemUser)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        List<Message> messages = messageService.getConversationMessages(conversation);
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }


    /**
     * Sends message to conversation.
     *
     * @param conversationId Id of conversation user wants to send message to.
     * @param messageText    Message to be sent.
     * @throws NotFoundException In case conversation with given Id does not exist.
     */
    @PutMapping(value = "/{conversationId}", consumes = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<Void> sendMessage(@PathVariable long conversationId, @RequestBody String messageText) throws NotFoundException {
        SystemUser systemUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());

        final Conversation conversation = messageService.getConversationById(conversationId);
        if (conversation == null) {
            throw new NotFoundException("Conversation with id " + conversationId + " has not been found.");
        }
        messageService.sendMessage(messageText, conversation, systemUser);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
