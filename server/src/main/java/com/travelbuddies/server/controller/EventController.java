package com.travelbuddies.server.controller;

import com.travelbuddies.server.commands.SearchEventRequestBody;
import com.travelbuddies.server.exception.NegativeRangeException;
import com.travelbuddies.server.model.Event;
import com.travelbuddies.server.model.SystemUser;
import com.travelbuddies.server.service.EventService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(path = "api/events/", produces = MediaType.APPLICATION_JSON_VALUE)
public class EventController {
    private static final Logger LOG = LoggerFactory.getLogger(EventController.class);

    private final EventService eventService;
    private final ControllerUtilities utilities;

    @Autowired
    public EventController(EventService eventService, ControllerUtilities utilities) {
        this.eventService = eventService;
        this.utilities = utilities;
    }

    @GetMapping
    public List<Event> getAllEvents() {
        return eventService.getAll();
    }

    @GetMapping(value = "my")
    public ResponseEntity<List<Event>> getAllLoggedUserEvents() {
        SystemUser systemUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());
        LOG.info("User with id {} is asking for all his events", systemUser.getId());

        return new ResponseEntity<>(eventService.getAllUserEvents(systemUser), HttpStatus.OK);

    }

    @GetMapping(value = "my/cities/{city}")
    public ResponseEntity<List<Event>> getAllLoggedUserEventsToCity(@PathVariable String city) {
        SystemUser systemUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());
        LOG.info("User with id {} is asking for all his events in city {}", systemUser.getId(), city);

        return new ResponseEntity<>(eventService.getAllUserEventsToCity(systemUser, city), HttpStatus.OK);
    }

    @GetMapping(value = "my/countries/{country}")
    public ResponseEntity<List<Event>> getAllLoggedUserEventsToCountry(@PathVariable String country) {
        SystemUser systemUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());
        LOG.info("User with id {} is asking for all his events in country {}", systemUser.getId(), country);

        return new ResponseEntity<>(eventService.getAllUserEventsToCountry(systemUser, country), HttpStatus.OK);
    }


    @GetMapping(value = "/{id}")
    public ResponseEntity<Event> getEvent(@PathVariable Integer id) {
        SystemUser systemUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());
        LOG.info("User with id {} is asking for event with id {}", systemUser.getId(), id);

        final Event event = eventService.getEvent(id);

        Objects.requireNonNull(event, String.format("Event with id %d was not found.", id));

        /*//Not really sure, about this solution. What if I want to view the event to join?
        if (!(event.getAuthor().equals(systemUser) || event.getParticipants().contains(systemUser))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }*/

        return new ResponseEntity<>(event, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Event> deleteEvent(@PathVariable Integer id) {
        SystemUser systemUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());
        LOG.info("User with id {} is asking to delete event with id {}", systemUser.getId(), id);

        final Event event = eventService.getEvent(id);
        Objects.requireNonNull(event, String.format("Event with id %d was not found.", id));

        if (!(event.getAuthor().equals(systemUser))) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }
        eventService.deleteEvent(event);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createEvent(@RequestBody Event event) {
        SystemUser systemUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());

        event.setAuthor(systemUser);
        eventService.createEvent(event);
        LOG.info("User with id {} created event {}", systemUser.getId(), event);

        final HttpHeaders headers = ControllerUtilities.createLocationHeaderFromCurrentUri("/{id}", event.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Event> updateEvent(@RequestBody Event event) {
        SystemUser systemUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());
        if (!event.getAuthor().equals(systemUser)) {
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        eventService.updateEvent(event);
        LOG.info("User with id {} updated event {}", systemUser.getId(), event);

        final HttpHeaders headers = ControllerUtilities.createLocationHeaderFromCurrentUri("/{id}", event.getId());
        return new ResponseEntity<>(event, headers, HttpStatus.OK);
    }

    @PostMapping(value = "/search", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Event>> getEvents(@RequestBody SearchEventRequestBody searchEventRequestBody) {
        if (searchEventRequestBody.getRangeInDays() < 0)
            throw new NegativeRangeException();

        Calendar date = Calendar.getInstance();
        //calender month starts form 0
        date.set(searchEventRequestBody.getYear(), searchEventRequestBody.getMonth(), searchEventRequestBody.getDay(), 0, 0, 0);
        LOG.info("Returning events in {} on {}{}{} in range of {} days", searchEventRequestBody.getDestination(), searchEventRequestBody.getYear(), searchEventRequestBody.getMonth(), searchEventRequestBody.getDay(), searchEventRequestBody.getRangeInDays());

        return new ResponseEntity<>(eventService.searchEvents(date, searchEventRequestBody.getDestination(), searchEventRequestBody.getRangeInDays()), HttpStatus.OK);
    }

    @GetMapping(value = "/upcoming")
    public ResponseEntity<List<Event>> getUpcomingEvents() {
        SystemUser systemUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());
        LOG.info("User with id {} is asking for upcoming events", systemUser.getId());

        return new ResponseEntity<>(eventService.getAllUserUpcomingEvents(systemUser), HttpStatus.OK);
    }

    @GetMapping(value = "attend/{id}")
    public ResponseEntity<Event> getEvent(@PathVariable Long id) {
        SystemUser systemUser = utilities.GetUser((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());

        final Event event = eventService.attendEvent(id, systemUser);
        LOG.info("User with id {} was added to participants to event with id {}", systemUser.getId(), id);

        return new ResponseEntity<>(event, HttpStatus.OK);
    }
}