package com.travelbuddies.server.importer;

import com.travelbuddies.server.controller.EventController;
import com.travelbuddies.server.dao.CountryDao;
import com.travelbuddies.server.dao.DestinationDao;
import com.travelbuddies.server.model.Country;
import com.travelbuddies.server.model.Destination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.*;

@Service
public class CSVImporter {

    @Autowired
    private CountryDao countryDao;
    @Autowired
    private DestinationDao destinationDao;

    private static final Logger LOG = LoggerFactory.getLogger(CSVImporter.class);

    public List<String[]> readCSV(String path, String separator) {
        List<String[]> parsedCSV = new ArrayList<>();


        int rowNumber = 0;
        String row = "";
        try {
            BufferedReader csvReader = new BufferedReader(new FileReader(path));

            //Header check
            row = csvReader.readLine();
            int columns = row.split(separator).length;
            boolean inQuotation = false;

            LOG.info("Reading file \nHeader: " + row);


            //Parsing rows to columns
            while ((row = csvReader.readLine()) != null) {
                rowNumber++;
                int actualColumn = 0;
                String[] data = new String[columns];
                StringBuilder actualWord = new StringBuilder();
                for (char c : row.toCharArray()) {
                    if (c == '"')
                        inQuotation = !inQuotation;
                    else if (!inQuotation && c == separator.charAt(0)) {
                        data[actualColumn++] = actualWord.toString().trim();
                        actualWord = new StringBuilder();
                    } else
                        actualWord.append(c);
                }
                data[actualColumn] = actualWord.toString();

                parsedCSV.add(data);
            }
            csvReader.close();
        } catch (FileNotFoundException e) {
            LOG.error("Could not find file: " + path);
            return null;
        } catch (IOException e) {
            LOG.error(String.format( "Problem with reading file %s\n on row: %d. %s\n", path, rowNumber, row));
            return null;
        }
        LOG.error("successful\n");
        return parsedCSV;
    }

    public boolean writeDestinationsToDB(List<String[]> parsedCSV) {
        Dictionary<String, String> abbreviations = null;
        try {
            abbreviations = loadCountryAbbreviationsDictionary();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        if (abbreviations == null)
            return false;

        LOG.info("Writing to DB: ");

        int i = 0;
        int len = parsedCSV.size();
        double progress;
        try {
            for (String[] data : parsedCSV) {
                progress = (++i * 100) / (double) len;
                LOG.info(new DecimalFormat("##.##").format(progress) + "% " + Arrays.toString(data));
                ConvertCSVRowToEntities(data, abbreviations);
            }
        } catch (NumberFormatException e) {
            LOG.error(String.format("Problem with converting string to number.\nrow: %d. %s\n", i + 1, Arrays.toString(parsedCSV.get(i - 1))));
            return false;
        }
        return true;
    }

    private Dictionary<String, String> loadCountryAbbreviationsDictionary() throws URISyntaxException {
        LOG.info("Loading abbreviations to memory");
        Dictionary<String, String> abbreviations = new Hashtable<>();
        String path = Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource("Abbreviations.csv")).toURI()).toString();
        List<String[]> abbreviationsList = readCSV(path, ";");
        if (abbreviationsList == null)
            return null;
        for (String[] data : abbreviationsList) {
            abbreviations.put(data[2].toLowerCase(), data[0]);
        }
        return abbreviations;
    }

    /**/
    @Transactional
    public void ConvertCSVRowToEntities(String[] data, Dictionary<String, String> abbreviations) throws NumberFormatException {
        Country country = countryDao.findByName(data[1]);
        if (country == null) {
            country = new Country();
            country.setName(data[1]);
            country.setAbbreviation(abbreviations.get(country.getName()));
            countryDao.save(country);
        } else if (country.getAbbreviation() == null || !country.getAbbreviation().equals(abbreviations.get(country.getName().toLowerCase()))) {
            country.setAbbreviation(abbreviations.get(country.getName().toLowerCase()));
            countryDao.save(country);
        }

        int id = Integer.parseInt(data[3]);
        Destination destination = new Destination();
        destination.setCity(data[0]);
        destination.setCountry(country);
        destination.setSubcountry(data[2]);
        destination.setGeonameId(id);
        Destination foundDestination = destinationDao.findByGeonameId(id);
        if (foundDestination == null || !foundDestination.sameAs(destination))
            destinationDao.save(destination);

    }

    public boolean updateCountryDetails(List<String[]> parsedCSV) {
        for (String[] data : parsedCSV) {
            writeCountryDetailsToDB(data);
        }
        return true;
    }

    @Transactional
    public void writeCountryDetailsToDB(String[] data) {
        Country country = countryDao.findByAbbreviation(data[0]);
        if (country != null) {
            //header: "country_code","country_name","country_alpha3_code","country_numeric_code","capital","country_demonym","total_area","population","idd_code","currency_code","currency_name","currency_symbol","lang_code","lang_name","cctld"
            //          0               1               2                       3                   4           5               6           7               8       9               10              11                  12            13        14
            country.setCapital_city(data[4]);
            country.setCountry_demonym(data[5]);
            country.setTotal_area(data[6]);
            country.setPopulation(data[7]);
            country.setCurrency_code(data[9]);
            country.setCurrency_name(data[10]);
            country.setCurrency_symbol(data[11]);
            country.setLang_name(data[13]);
            countryDao.save(country);
        }
    }

}
