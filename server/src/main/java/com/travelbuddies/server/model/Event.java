package com.travelbuddies.server.model;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "event")
public class Event {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	long id;

	private String description;
	private Date fromDate;
	private Date toDate;

	@ManyToOne
	private Destination destination;
	@ManyToOne
	private SystemUser author;
	@ManyToMany
	private Set<SystemUser> participants = new HashSet<>();
	private boolean deleted = false;

	public long getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date from) {
		this.fromDate = from;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date to) {
		this.toDate = to;
	}

	public Destination getDestination() {
		return destination;
	}

	public void setDestination(Destination destination) {
		this.destination = destination;
	}

	public SystemUser getAuthor() {
		return author;
	}

	public void setAuthor(SystemUser author) {
		this.author = author;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public Set<SystemUser> getParticipants() {
		return participants;
	}

	public void setParticipants(Set<SystemUser> participants) {
		this.participants = participants;
	}
}