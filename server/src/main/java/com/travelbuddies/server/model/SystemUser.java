package com.travelbuddies.server.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "systemuser")
public class SystemUser {
    @JsonProperty("sub")
    @Id
    String id;
    @JsonProperty("given_name")
    String givenName;
    @JsonProperty("family_name")
    String familyName;
    String nickname;
    String name;
    String picture;
    @JsonProperty("updated_at")
    String updatedAt;
    String email;
    @JsonProperty("email_verified")
    boolean emailVerified;
    @JsonIgnore
    @ManyToMany
    Set<SystemUser> friends;
    @ManyToMany
    Set<Country> visitedCountries;


    public String getId() {
        return id;
    }

    public void setId(String sub) {
        this.id = sub;
    }

    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailVerified() {
        return emailVerified;
    }

    public void setEmailVerified(boolean emailVerified) {
        this.emailVerified = emailVerified;
    }

    public Set<SystemUser> getFriends() {
        return friends;
    }

    public void setFriends(Set<SystemUser> friends) {
        this.friends = friends;
    }

    public Set<Country> getVisitedCountries() {
        return visitedCountries;
    }

    public void setVisitedCountries(Set<Country> visitedCountries) {
        this.visitedCountries = visitedCountries;
    }

    public void addFriend(SystemUser user) {
        Objects.requireNonNull(user);
        if (friends == null) {
            this.friends = new HashSet<>();
        }
        friends.add(user);
    }

    public void removeFriend(SystemUser user) {
        Objects.requireNonNull(user);
        if (friends == null) {
            return;
        }
        friends.removeIf(u -> Objects.equals(u.getId(), user.getId()));
    }

    public void addVisitedCountry(Country country) {
        Objects.requireNonNull(country);
        if (visitedCountries == null) {
            this.visitedCountries = new HashSet<>();
        }
        visitedCountries.add(country);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SystemUser that = (SystemUser) o;
        return emailVerified == that.emailVerified &&
                Objects.equals(id, that.id) &&
                Objects.equals(givenName, that.givenName) &&
                Objects.equals(familyName, that.familyName) &&
                Objects.equals(nickname, that.nickname) &&
                Objects.equals(name, that.name) &&
                Objects.equals(picture, that.picture) &&
                Objects.equals(updatedAt, that.updatedAt) &&
                Objects.equals(email, that.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, givenName, familyName, nickname, name, picture, updatedAt, email, emailVerified);
    }
}
