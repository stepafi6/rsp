package com.travelbuddies.server.model;

import org.springframework.context.annotation.Lazy;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "conversation")
public class Conversation{
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	long id;
	@ManyToMany
	public List<SystemUser> systemUsers;
	@Lazy
	@OneToMany(cascade = CascadeType.ALL)
	public List<Message> messages;

	public long getId() {
		return id;
	}

	public List<SystemUser> getSystemUsers() {
		return systemUsers;
	}

	public void setSystemUsers(List<SystemUser> systemUsers) {
		this.systemUsers = systemUsers;
	}

	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}

	public void addMessage(Message message) {
		Objects.requireNonNull(message);
		if (messages == null) {
			this.messages = new ArrayList<>();
		}
		messages.add(message);
	}
}