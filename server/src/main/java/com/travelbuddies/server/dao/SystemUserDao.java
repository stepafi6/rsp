package com.travelbuddies.server.dao;

import com.travelbuddies.server.model.SystemUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SystemUserDao extends JpaRepository<SystemUser,String> {
    List<SystemUser> findByGivenNameContainingOrFamilyNameContainingIgnoreCase(String query, String query2);
}
