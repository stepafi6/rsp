package com.travelbuddies.server.dao;

import com.travelbuddies.server.model.Conversation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConversationDao extends JpaRepository<Conversation,Long> {
}
