package com.travelbuddies.server.dao;

import com.travelbuddies.server.model.Destination;
import com.travelbuddies.server.model.Event;
import com.travelbuddies.server.model.SystemUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface EventDao extends JpaRepository<Event, Long> {
    List<Event> findByParticipantsContainingIgnoreCaseOrAuthor(SystemUser user,SystemUser auth);
    List<Event> findAllByParticipantsContainingIgnoreCaseAndFromDateAfterOrAuthorAndFromDateAfterOrderByFromDateAsc(SystemUser user, Date date, SystemUser auth, Date date1);
    List<Event> findAllByDestinationAndFromDateBetween(Destination destination, Date fromDateStart, Date fromDateEnd);
    List<Event> findAllByDestinationAndParticipants(Destination destination,SystemUser participant);
    @Query("SELECT e FROM Event e WHERE e.destination.country = ?1 AND (e.author = ?2 OR ?2 member e.participants)")
    List<Event> findAllByDestination_Country_WhereIsUser(String country, SystemUser participant);
    @Query("SELECT e FROM Event e WHERE e.destination.city = ?1 AND (e.author = ?2 OR ?2 member e.participants)")
    List<Event> findAllByDestination_CityWhereIsUser(String city, SystemUser participant);
}
