package com.travelbuddies.server.dao;

import com.travelbuddies.server.model.Destination;
import org.springframework.data.elasticsearch.repository.ElasticsearchCrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DestinationDao extends ElasticsearchCrudRepository<Destination, Long> {

    List<Destination> findTop20ByCityContainingIgnoreCase(String city);

    List<Destination> findTop20BySubcountryContainingIgnoreCase(String subcountry);

    Destination findByGeonameId(int id);

}