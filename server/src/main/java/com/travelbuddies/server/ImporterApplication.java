package com.travelbuddies.server;

import com.travelbuddies.server.importer.CSVImporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.util.StopWatch;

import java.util.List;

@SpringBootApplication
public class ImporterApplication
        implements CommandLineRunner {
    private final String HEADER = "name,country,subcountry,geonameid";
    private final String DEFAULT_SEPARATOR = ",";

    private static final Logger LOG = LoggerFactory.getLogger(ImporterApplication.class);

    @Autowired
    CSVImporter csvImporter;

    @Autowired
    private ConfigurableApplicationContext context;

    public static void main(String[] args) {
        SpringApplication.run(ImporterApplication.class, args);
    }

    @Override
    public void run(String... args) {
        StopWatch watch = new StopWatch();
        watch.start();
        String separator = DEFAULT_SEPARATOR;
        if (args.length == 2) {
            LOG.info(String.format("Using default separator: '%s'\n", DEFAULT_SEPARATOR));

        } else if (args.length == 3) {
            separator = args[2];
        } else {
            LOG.error("Wrong number of arguments.\nExpected: <-destinations/-details> <path> <separator>");
            return;
        }
        if (!(args[0].equals("-details") || args[0].equals("-destinations"))) {
            LOG.info("Unknown first argument.\nExpected: <-destinations/-details> <path> <separator>");
            context.close();
            return;
        }

        List<String[]> parsedCSV = csvImporter.readCSV(args[1], separator);
        if (args[0].equals("-details")) {
            if (parsedCSV != null && csvImporter.updateCountryDetails(parsedCSV)) {
                LOG.info("Successfully updated");
            }
        } else if (parsedCSV != null && csvImporter.writeDestinationsToDB(parsedCSV))
            LOG.info("Successfully imported");

        watch.stop();
        LOG.info("Total time: " + ((int) (watch.getTotalTimeSeconds() / 3600) > 0 ? (int) (watch.getTotalTimeSeconds() / 3600) + "h " : "") + ((int) (watch.getTotalTimeSeconds() / 60) > 0 ? (int) (watch.getTotalTimeSeconds() / 60) + "m " : "") + watch.getTotalTimeSeconds() % 60 + "s");
        context.close();
    }

}
