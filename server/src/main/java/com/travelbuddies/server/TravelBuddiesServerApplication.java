package com.travelbuddies.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class TravelBuddiesServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelBuddiesServerApplication.class, args);
	}

}
