package com.travelbuddies.server.service;

import com.travelbuddies.server.dao.ConversationDao;
import com.travelbuddies.server.dao.MessageDao;

import com.travelbuddies.server.model.Conversation;
import com.travelbuddies.server.model.Message;
import com.travelbuddies.server.model.SystemUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class MessageService {

    private final ConversationDao conversationDao;

    @Autowired
    public MessageService(ConversationDao conversationDao) {
        this.conversationDao = conversationDao;
    }

    /**
     * Adds message to specified conversation.
     * @param conversation Conversation to which the message should be added.
     * @param messageText Message to be added to the conversation.
     */
    @Transactional
    public void sendMessage(String messageText, Conversation conversation, SystemUser systemUser){
        Objects.requireNonNull(messageText);
        Objects.requireNonNull(conversation);
        Objects.requireNonNull(systemUser);

        Message message = new Message();

        message.setTimestamp(new Timestamp(System.currentTimeMillis()));
        message.setSystemUser(systemUser);
        message.setContent(messageText);

        conversation.addMessage(message);
        conversationDao.save(conversation);
    }

    /**
     * Creates new conversation with specified users.
     * @param users List of SystemUsers which the new conversation should be added to.
     */
    @Transactional
    public void createConversation(List<SystemUser> users) {
        Objects.requireNonNull(users);
        final Conversation conversation = new Conversation();
        conversation.setSystemUsers(users);
        conversationDao.save(conversation);
    }

    /**
     * Returns list of conversations of the user.
     * @param user User whose conversations we want to get.
     * @return List of conversations which is the specified user part of.
     */
    @Transactional
    public List<Conversation> getUserConversations(SystemUser user) {
        Objects.requireNonNull(user);
        final List<Conversation> toReturn = new ArrayList<>();
        List<Conversation> conversations = conversationDao.findAll();
        for(Conversation c : conversations) {
            if(c.getSystemUsers().contains(user)) {
                toReturn.add(c);
            }
        }
        return toReturn;
    }

    /**
     * Returns list of messages belonging to the conversation.
     * @param conversation Conversation which we want to get messages from.
     * @return List of messages which belong to specified conversation.
     */
    @Transactional
    public List<Message> getConversationMessages(Conversation conversation) {
        Objects.requireNonNull(conversation);
        if(conversation.getMessages() == null) {
            conversation.setMessages(new ArrayList<>());
        }
        return conversation.getMessages();
    }

    /**
     * Returns conversation with given id.
     * @param id Id of conversation to get.
     * @return Conversation with given id or null object.
     */
    @Transactional
    public Conversation getConversationById(long id) {
        return conversationDao.findById(id).orElse(null);
    }
}
