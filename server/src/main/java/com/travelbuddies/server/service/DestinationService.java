package com.travelbuddies.server.service;

import com.travelbuddies.server.dao.DestinationDao;
import com.travelbuddies.server.model.Destination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class DestinationService {


    private final DestinationDao destinationDao;

    @Autowired
    public DestinationService(DestinationDao destinationDao) {
        this.destinationDao = destinationDao;
    }


    /**
     * Returns cities which contain parameter
     * @param city String to be looked for in city name
     * @return List of cities which contain param String
     */
    @Transactional
    public List<Destination> autocompleteDestinationCity(String city) {
        List<Destination> result = destinationDao.findTop20ByCityContainingIgnoreCase(city);
        result.sort((o1, o2) -> o1.getCity().compareTo(o2.getCity()));
        return result;
    }

    /**
     * Returns sub-countries which contain parameter
     * @param subcountry String to be looked for in sub-country name
     * @return List of sub-countries which contain param String
     */
    @Transactional
    public List<Destination> autocompleteDestinationSubCountry(String subcountry) {
        List<Destination> result = destinationDao.findTop20BySubcountryContainingIgnoreCase(subcountry);
        result.sort((o1, o2) -> o1.getSubcountry().compareTo(o2.getSubcountry()));
        return result;
    }

    /**
     * Returns destinations which contain parameter
     * @param destination String to be looked for in sub-country name or city name
     * @return List of destinations which contain param String
     */
    @Transactional
    public List<Destination> autocompleteDestination(String destination) {
        List<Destination> destinations = new ArrayList<>();
        destinations.addAll(autocompleteDestinationCity(destination));
        destinations.addAll(autocompleteDestinationSubCountry(destination));
        return destinations;
    }
}


