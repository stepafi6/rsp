# Travel buddies

## Implementované požadavky

### Design pattern

* IoC - Server - Použití Springu
* Dependency injection - Server - např. DestinationService
* Factory - Android client - generování UI elementů (factory package)
* Provider - Android client - BackendBridge package
* Proxy - např. Android client - BackendBrigde package
* Interceptor - Android client - Authentication
* Listener - Android client UI on click
* Singleton - Global context Android client
* Optional - UI data z serveru

### Výběr vhodné technologie a jazyka

Projekt se dělí na tři části. Server, android klient a webový klient. Server je postaven na Java 8 Spring. Mobilní klient staví na android SDK, jde o tenký klient, který pouze konzumuje Rest API ze serveru. Webový klient je postaven na js frameworku Vue.js.

### Využití společné DB

Všechny tři moduly sdílí stejnou databázi.

### Využití cache

Server - CountryDAO. Využívá interface HazelcastRepository.

### Využití messaging principu

Není implementováno.

### Zabezpečení

Aplikace využívá na všech třech komponentách zabezpečení na principu OAuth2 od společnosti [Auth0](https://auth0.com). Jedná se o kompletního poskytovatele správe identity.

Př1: Při návštěvě adresy [tb-client.stepanek.app](https://tb-client.stepanek.app) jsme okamžitě vyzváni k autentizaci.

Př2: SecurityConfig třída na serveru

### Využití interceptors

Android client - při authentikaci, token bearer (package utils)

### Využití technologie REST

Server poskytuje REST rozhraní s kterým komunikují klientské aplikace.

### Nasazení na produkční server

Nasazujeme pomocí technologie Docker na virtual private server. Webový klient je dostupný na adrese [tb-client.stepanek.app](https://tb-client.stepanek.app). Server na obodné [tb-server.stepanek.app](https://tb-server.stepanek.app).

### Inicializační postup

Deploy serveru a webového klienta probíhá velmi podobně. V root složce projektu je umístěn soubor `deploy.bat`, který vytvoří z lokálních souborů nový (latest) docker image a pushne ho na náš privátní repozitář. Následně je na serveru spuštěn skript který zastaví a vymaže příslušný kontajner a spustí nový z nového image.

### Využití elasticksearch

Server - DestinationDao. Využívá interface ElasticsearchCrudRepository.

### Use cases

1. Vytvoření eventu
2. Sdílení eventu
3. Zobrazit mapu světa
4. Zobrazit navštívené země
5. Zobrazit chat
6. Posílat zprávy v chatu
7. Změnit heslo
8. Inicializační email
10. Registrace/Login
11. Zobrazení eventů
12. Přidat přítele
13. Odebrat přítele


### Cloudové služby

Jako ekvivalent cloudových služeb využíváme VPS server, zejména kvůli nižší cenně a eliminaci obav z astronimického vyúčtování. K samotnému běhu na VPS je využit Docker.